﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnController : MonoBehaviour
{
    private static RespawnController _instance;
    public static RespawnController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (RespawnController)FindObjectOfType(typeof(RespawnController));
                if (_instance == null)
                {
                    _instance = new GameObject().AddComponent<RespawnController>();
                }
            }
            return _instance;
        }
    }
    public event Action<Vector2> SpawnChange = delegate { };
    [SerializeField]
    GameObject _player;
    playerHealth _ph;
    Vector3 respawnPosition;
    Scene loadedLevel;
    private void Awake()
    {
        if (_player is null)
            _player = GameObject.FindGameObjectWithTag(Tags.Player);
        if(_ph is null)
            _ph = _player.GetComponent<playerHealth>();
    }
    private void Start()
    {
        loadedLevel=SceneManager.GetActiveScene();
    }
    public void RespawnPlayer()
    {
        
        if (respawnPosition == Vector3.zero)
        {
            _player.transform.position = _ph.startPosition;
            
            //SceneManager.LoadScene(loadedLevel.buildIndex);
        }
        else
        {
            _player.transform.position = respawnPosition;
        }
        _ph.Restore();
    }
    public Vector2 GetCurrentRespawn()
    {
        return (Vector2)respawnPosition;
    }

    public void SetRespawn(Transform check)
    {
        if ((Vector2)respawnPosition != (Vector2)check.position)
        {       
            respawnPosition = check.position;
            respawnPosition.z = _ph.startPosition.z;
            SpawnChange((Vector2)check.position);
        }
    }
}
