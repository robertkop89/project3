﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aimRopeArm : MonoBehaviour
{
    
    [SerializeField]
    Movement2D movementSystem;
    bool isActive;
    public Vector2 position;

    [SerializeField]
    private Animator _anim;
    [SerializeField]
    [Range(0f, 1f)]
    private float IkWeight;
    public bool inHook;
    //[SerializeField]
    //BoxMovement boxMove;
    //public bool aiming;


    // Update is called once per frame
    void Update()
    {
        if (movementSystem.isSwinging)
        {
            isActive = true;
            position= movementSystem.ropeHook;
        }
        
        else
        {
            isActive = false;
        }
    }
    private void OnAnimatorIK(int layerIndex)
    {
        if (isActive)
        {
            _anim.SetIKPositionWeight(AvatarIKGoal.RightHand, IkWeight);
            _anim.SetIKPosition(AvatarIKGoal.RightHand, position);
        }
        else if (inHook)
        {
            _anim.SetIKPositionWeight(AvatarIKGoal.RightHand, IkWeight);
            _anim.SetIKPosition(AvatarIKGoal.RightHand, position);
        }
        //else if (aiming)
        //{
        //    _anim.SetIKPositionWeight(AvatarIKGoal.RightHand, IkWeight);
        //    _anim.SetIKPosition(AvatarIKGoal.RightHand, position);
        //}
        else
        {
            _anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("in collision");
        if (collision.CompareTag(Tags.Hook))
        {
            inHook = true;
            position = collision.transform.position;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Hook))
        {
            inHook = false;
        }
    }
}
