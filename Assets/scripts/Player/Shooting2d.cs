﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting2d : MonoBehaviour
{
    [Tooltip("Delay between shoots")]
    [SerializeField]
    float fireCooldown = 2,lrHeight=0.5f;
    private float fireTimer;
    [Tooltip("Bullet fired by the player")]
    [SerializeField]
    GameObject BulletPrefab;
    [Tooltip("Position where the bullet is fired from")]
    [SerializeField]
    Transform FirePosition;
    Transform bullet;
    //[SerializeField] LineRenderer lr;
    private Camera mainCamera;
    //public LayerMask mouseAimMask;
    public Transform targetTransform;
    [Tooltip("Animator needed for Inverse Kinematics (shooting arm control")]
    [SerializeField]
    private Animator _anim;
    public Vector2 offset;
    [Tooltip("Crosshair game object tied to the player")]
    [SerializeField]
    private GameObject Crosshair;
    public Vector3 mouse;
    [SerializeField]
    private float crosshairDistanceMultiplier;
    private bool fire;
    [Tooltip("Weight value used to determine how strongly Inverse Kinematic should affect the left hand")]
    [SerializeField]
    [Range(0f, 1f)]
    private float IkWeight;
    public bool inHook;
    public Transform hookPosition;
    private GameObject alternativeTarget;
    private Vector3 aimhead= new Vector3(300, 0.7f, 0.1f);
    [SerializeField]
    AudioSource sundSource;



    // Start is called before the first frame update
    void Start()
    {
        //MusicController.Instance.TestMethod();
        fireTimer = 0;
        //lr = GetComponent<LineRenderer>();
        alternativeTarget = new GameObject();
        alternativeTarget.transform.SetParent(transform.parent.transform);
        mainCamera = Camera.main;
        Crosshair.transform.localPosition = new Vector3(300, 0, 0.1f);
        targetTransform = Crosshair.transform;
    }
    private int FacingSign
    {
        get
        {
            Vector3 perp = Vector3.Cross(transform.forward, Vector3.forward);
            float dir = Vector3.Dot(perp, transform.up);
            return dir > 0f ? -1 : dir < 0f ? 1 : 0;


        }
    }
    

    // Update is called once per frame
    void Update()
    {
        /*Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mouseAimMask))
        {
            targetTransform.position = hit.point;
        }*/
        Vector3 aim = Vector3.zero; //Sad :(
           //new Vector3(Input.GetAxis("HorizontalAim")*FacingSign, Input.GetAxis("VerticalAim"),0); 
        //if (aim.magnitude > 0.5f)
        //{
        //    aim.Normalize();
        //    aim *= crosshairDistanceMultiplier;
        //    aim += Vector3.forward*0.15f;
        //    targetTransform = Crosshair.transform;
        //    Crosshair.transform.localPosition = aim;
        //    Crosshair.SetActive(true);
            
        //} else 
        if (inHook)
        {
            alternativeTarget.transform.localPosition = aimhead;
            targetTransform = alternativeTarget.transform;
            Crosshair.transform.position = new Vector3(hookPosition.position.x,hookPosition.position.y,transform.position.z);
            Crosshair.SetActive(true);
        }
        else {
            targetTransform = Crosshair.transform;
            Crosshair.SetActive(false);
            Crosshair.transform.localPosition = aimhead;
            //targetTransform.position = transform.position+transform.right*2;
        }
        mouse = Input.mousePosition;
        /*targetTransform.position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(transform.localPosition);
        offset = new Vector2(mouse.x - screenPoint.x, mouse.y - screenPoint.y);*/
        /*Vector3 direction = targetTransform.position - FirePosition.position;
        Quaternion toRotation = Quaternion.FromToRotation(FirePosition.up, direction);
        FirePosition.rotation = Quaternion.Lerp(FirePosition.rotation, toRotation, 0.5f)*/;
        FirePosition.LookAt(targetTransform);
        FirePosition.RotateAround(FirePosition.position, FirePosition.up, -90);
        if (Input.GetButtonDown("Fire2"))
        {
            fire = true;
        }else if (Input.GetAxis("Fire2") > 0)
        {
            fire = true;
        }
        if(fire)
        {
            fire = false;
            if ( fireTimer < Time.time)
            {
                _anim.SetTrigger("Shoot");
                fireTimer = Time.time + fireCooldown;
                sundSource.Play();
                if (aim.magnitude > 0.1f)
                {
                    switch (FacingSign)
                    {
                        case -1:
                            bullet = Instantiate(BulletPrefab, FirePosition.position, Quaternion.Euler(0, 180, 0)).transform;
                            break;
                        case 1:
                            bullet = Instantiate(BulletPrefab, FirePosition.position, Quaternion.Euler(0, 0, 0)).transform;
                            break;
                    }
                }
                else
                {
                    bullet = Instantiate(BulletPrefab, FirePosition.position, FirePosition.rotation).transform;
                }
            }
            /*else
            {
                if (bullet)
                    Destroy(bullet.gameObject);
            }*/
        }
    }
    private void OnAnimatorIK()
    {
            _anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, IkWeight);
            _anim.SetIKPosition(AvatarIKGoal.LeftHand, targetTransform.position);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(FirePosition.position, FirePosition.right*10);
    }
    /*
    private void LateUpdate()
    {
        if (bullet != null)
        {
            lr.positionCount = 2;
            Vector3 lrposition = transform.position;
            lrposition.y = FirePosition.position.y;
            lr.SetPosition(0, lrposition);
            lr.SetPosition(1, bullet.position);
        }
        else
        {
            lr.positionCount = 0;
        }
    }
    */
}
