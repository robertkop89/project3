﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FetchVelocity : MonoBehaviour
{
    GameObject player;
    Rigidbody2D player_rb;
    public Text textField;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(Tags.Player);
        player_rb = player.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (textField == null)
        {
            Debug.Log("SET textField for" + name);
            return;
        }
        textField.text = player_rb.velocity.x.ToString();
    }
}
