﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherShooting : MonoBehaviour
{
    [SerializeField]
    float fireCooldown = 2, lrHeight = 0.5f;
    float fireTimer;
    [SerializeField]
    GameObject BulletPrefab;
    [SerializeField]
    Transform FirePosition;
    Transform bullet;
    [SerializeField] GameObject playerLocation;
    //LineRenderer lr;

    // Start is called before the first frame update
    void Start()
    {
        fireTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (bullet is null && fireTimer < Time.time)
            {
                fireTimer = Time.time + fireCooldown;
                bullet = Instantiate(BulletPrefab, FirePosition.position, FirePosition.rotation).transform;
                bullet.GetComponent<TieBullet>().PullLocation = playerLocation;

            }
            else
            {
                if (bullet)
                    Destroy(bullet.gameObject);
            }
        }
    }
    /*
    private void LateUpdate()
    {
        if (bullet != null)
        {
            lr.positionCount = 2;
            Vector3 lrposition = transform.position;
            lrposition.y = FirePosition.position.y;
            lr.SetPosition(0, lrposition);
            lr.SetPosition(1, bullet.position);
        }
        else
        {
            lr.positionCount = 0;
        }
    }
    */
}
