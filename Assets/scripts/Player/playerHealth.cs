﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerHealth : MonoBehaviour
{
    [SerializeField]
    private Movement2D playerMovement;
    [SerializeField]
    private Image[] hearts;
    [SerializeField]
    private Sprite fullHeart;
    [SerializeField]
    private Sprite emptyHearth;
    public int health { get; private set; }
    [SerializeField]
    private int maxHealth=5;
    public Vector3 startPosition { get; private set; }
    [SerializeField]
    public float damageCooldown = 1;
    private float damageTimer;
    Animator _anim;
    [SerializeField]
    AudioSource damageAudio;
    [SerializeField]
    AudioSource deathAudio;
    // Start is called before the first frame update
    private void Start()
    {
        _anim = playerMovement.GetAnimator();
        startPosition =gameObject.transform.position;
        health = maxHealth;
        for (int i = 0; i < hearts.Length; ++i)
        {
            hearts[i].sprite = fullHeart; 
        }
    }
    public void Restore()
    {
        health = maxHealth;
        for (int i = 0; i < hearts.Length; ++i)
        {
            hearts[i].sprite = fullHeart;
        }
        _anim.SetTrigger("Restore");
        playerMovement.isAlive = true;
    }

    public void TakeDamage(Transform source)
    {
        if (damageTimer > Time.time)
            return;
        else
            damageTimer = Time.time + damageCooldown;
        if (!(source is null))
        {
            damageAudio.Play();
            playerMovement.Knockback(source);
            if (source.position.x >= transform.position.x)
            {
                if (FacingSign >= 0)
                {
                    _anim.SetTrigger("DamageFront");
                }
                else
                {
                    _anim.SetTrigger("DamageBack");
                }
            }
            else
            {
                if (FacingSign >= 0)
                {
                    _anim.SetTrigger("DamageBack");
                }
                else
                {
                    _anim.SetTrigger("DamageFront");
                }
            }
            if (--health <= 0)
            {
                deathAudio.Play();
                hearts[0].sprite = emptyHearth;
                playerMovement.DEATH();
                
            }
            else
            {
                for (int i = health; i < hearts.Length; ++i)
                {
                    hearts[i].sprite = emptyHearth;
                }
            }
        }
        else
        {
            deathAudio.Play();
            health = 0;
            _anim.SetTrigger("DamageFront");
            for (int i = health; i < hearts.Length; ++i)
            {
                hearts[i].sprite = emptyHearth;
            }
            playerMovement.DEATH();
        }
    }

    private int FacingSign
    {
        get
        {
            Vector3 perp = Vector3.Cross(transform.forward, Vector3.forward);
            float dir = Vector3.Dot(perp, transform.up);
            return dir > 0f ? -1 : dir < 0f ? 1 : 0;


        }
    }
}
