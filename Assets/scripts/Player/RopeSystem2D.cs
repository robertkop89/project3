﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSystem2D : MonoBehaviour
{
    public GameObject ropeHingeAnchor;
    public DistanceJoint2D ropeJoint;
    public Collider2D playerCollider;
    //public Transform crosshair;
    [SerializeField]
    public Movement2D _movemnt2D;
    private bool ropeAttached;
    //private Vector2 playerPosition;
    private Rigidbody2D ropeHingeAnchorRb;
    private SpriteRenderer ropeHingeAnchorSprite;
    public LineRenderer ropeRenderer;
    public LayerMask ropeLayerMask;
    public float ropeMaxCastDistance = 40f;
    public float rope_teleport_distance_multiplier=0.1f;
    public List<Vector2> ropePositions = new List<Vector2>();
    private bool distanceSet;
    private Dictionary<Vector2, int> wrapPointsLookup = new Dictionary<Vector2, int>();
    [Header("Climbing speed")]
    public float specialClimbSpeed = 6f;
    public float climbSpeed = 3f;
    private bool isColliding;
    private bool inHook = false;
    private GameObject hookPoint;
    [SerializeField]
    float ropeMinLength=3;
    [SerializeField]
    Transform ropeArm;
    Rigidbody2D ownBody;
    [SerializeField]
    private float ropeRealseVelocityMultiplier = 2f;
    [SerializeField]
    aimRopeArm _armAim;
    [SerializeField]
    Shooting2d _shot2d;
    Vector3 aim;
    [SerializeField]
    BoxMovement _BoxMovement;
    [SerializeField]
    AudioSource hookSound;

    // Start is called before the first frame update
    void Start()
    {
        ownBody = GetComponent<Rigidbody2D>();
        if (ropeArm is null)
            ropeArm = transform;
        ropeAttached = false;
        ropeJoint.enabled = false;
        ropeHingeAnchorRb = ropeHingeAnchor.GetComponent<Rigidbody2D>();
        ropeHingeAnchorSprite = ropeHingeAnchor.GetComponent<SpriteRenderer>();
        ResetRope();

    }

    // Update is called once per frame
    void Update()
    {
        if (ropeAttached)
        {
            _movemnt2D.isSwinging = true;
            _movemnt2D.ropeHook = ropePositions.Last();
            if (ropePositions.Count > 0)
            {
                // 2
                var lastRopePoint = ropePositions.Last();
                var playerToCurrentNextHit = Physics2D.Raycast(transform.position, (lastRopePoint - (Vector2)transform.position).normalized, Vector2.Distance(transform.position, lastRopePoint) - 0.1f, ropeLayerMask);
                
                // 3
                if (playerToCurrentNextHit)
                {
                    var colliderWithVertices = playerToCurrentNextHit.collider as PolygonCollider2D;
                    if (!(colliderWithVertices is null))
                    {
                        var closestPointToHit = GetClosestColliderPointFromRaycastHit(playerToCurrentNextHit, colliderWithVertices);
                        // 4
                        if (wrapPointsLookup.ContainsKey(closestPointToHit))
                        {
                            ResetRope();
                            return;
                        }
                        // 5
                        ropePositions.Add(closestPointToHit);
                        wrapPointsLookup.Add(closestPointToHit, 0);
                        distanceSet = false;
                    }
                }
            }
        } else
            _movemnt2D.isSwinging = false;
        if (Input.GetButtonDown("Jump"))
        {
            if (ropeAttached)
            {
                ResetRope();
                ownBody.velocity = ropeRealseVelocityMultiplier * ownBody.velocity;
            }
        }
        if (inHook)
        {
            if ((Input.GetAxis("Rope")<0 || Input.GetButtonDown("Rope")) && !ropeAttached)
            {
                hookSound.Play();
                if (ropeAttached) return;
                ropeRenderer.enabled = true;
                Vector3 diagonal= hookPoint.transform.position - transform.position;
                var hit = Physics2D.Raycast(transform.position, (Vector2)diagonal, ropeMaxCastDistance, ropeLayerMask);
                if (hit.collider != null)
                {
                    ropeAttached = true;
                    if (!ropePositions.Contains(hit.point)){
                        diagonal.z = 0;
                        transform.position += diagonal* rope_teleport_distance_multiplier;
                        //transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 20f), ForceMode2D.Impulse);
                        ropePositions.Add(hit.point);
                        //Collider2D hitObjectCollider = hit.collider;
                       // playerCollider.enabled = false;
                        //hitObjectCollider.enabled = false;

                        
                        ropeJoint.distance = Vector2.Distance(transform.position, hit.point);
                        ropeJoint.enabled = true ;

                        //playerCollider.enabled = true;
                        //hitObjectCollider.enabled = true;

                        ropeHingeAnchorSprite.enabled=true;
                    }
                }
                else
                {
                    Debug.Log(transform.position.ToString()+" huh "+((Vector2)hookPoint.transform.position - (Vector2)transform.position));
                    ropeRenderer.enabled = false;
                    ropeAttached = false;
                    ropeJoint.enabled = false;
                }
            }
            
            //HandleRopeLength();
            UpdateRopePositions();
            HandleRopeUnwrap();
        }
        else
        {
            aim = new Vector3(Input.GetAxis("HorizontalAim"), Input.GetAxis("VerticalAim"), 0);
        }
        
    }
    private void FixedUpdate()
    {
        HandleRopeLength();
    }
    private void HandleRopeLength()
    {
        if(ropeAttached && !inHook)
        {
            ropeJoint.distance -= Time.deltaTime * specialClimbSpeed;
        }
        // 1
        if ((Input.GetAxis("RopeSize") >= 1f )&& ropeAttached && !isColliding)
        {
            ropeJoint.distance -= Time.deltaTime * climbSpeed;
        }
        else if (Input.GetAxis("RopeSize") < 0f && ropeAttached && inHook)
        {
            ropeJoint.distance += Time.deltaTime * climbSpeed;
        }
        if (_movemnt2D.groundCheck)
        {
            if(ropeJoint.distance>ropeMinLength)
            ropeJoint.distance -= Time.deltaTime * specialClimbSpeed;
        }
    }
    private void ResetRope()
    {
        ropeJoint.enabled = false;
        ropeAttached = false;
        _movemnt2D.isSwinging = false;
        ropeRenderer.positionCount = 2;
        ropeRenderer.SetPosition(0, ropeArm.position);
        ropeRenderer.SetPosition(1, ropeArm.position);
        ropePositions.Clear();
        ropeHingeAnchorSprite.enabled = false;
        wrapPointsLookup.Clear();
        
    }
    
    private void OnCollisionStay(Collision collision)
    {
        isColliding = true;   
    }
    private void OnCollisionExit(Collision collision)
    {
        isColliding = false;   
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        
        if (collision.CompareTag(Tags.Hook))
        {
            
            inHook = true;
            hookPoint = collision.gameObject;
            if (!(_armAim is null))
            {
                _armAim.inHook = true;
                _armAim.position = collision.transform.position;

            }
            if (!(_shot2d is null))
            {
                _shot2d.inHook = true;
                _shot2d.hookPosition = collision.transform;
            }
        }
        if (ropeAttached && !collision.CompareTag(Tags.Hook))
        {
            isColliding = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Hook))
        {
            inHook = false;
            hookPoint = null;
            if (!(_armAim is null))
            {
                _armAim.inHook = false;

            }
            if (!(_shot2d is null))
            {
                _shot2d.inHook = true;
            }
        }
        if (ropeAttached && !(collision.CompareTag(Tags.Hook)))
        {
            isColliding = false;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(Tags.Hook))
        {
            inHook = true;
            hookPoint = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag(Tags.Hook))
        {
            inHook = false;
            hookPoint = null;
        }
    }
    private Vector2 GetClosestColliderPointFromRaycastHit(RaycastHit2D hit, PolygonCollider2D polyCollider)
    {
        var distanceDictionary = polyCollider.points.ToDictionary<Vector2, float, Vector2>(
            position => Vector2.Distance(hit.point, polyCollider.transform.TransformPoint(position)),
            position => polyCollider.transform.TransformPoint(position));
        var orderedDictionary = distanceDictionary.OrderBy(e => e.Key);
        return orderedDictionary.Any() ? orderedDictionary.First().Value : Vector2.zero;
    }
    private void UpdateRopePositions()
    {
        if (!ropeAttached)
        {
            return;
        }
        ropeRenderer.positionCount = ropePositions.Count + 1;
        for (var i = ropeRenderer.positionCount - 1; i >= 0; i--)
        {
            if (i != ropeRenderer.positionCount - 1) // if not the Last point of line renderer
            {
                Vector3 target = new Vector3(ropePositions[i].x, ropePositions[i].y,ropeHingeAnchor.transform.position.z);
                ropeRenderer.SetPosition(i, target);
                if (i == ropePositions.Count - 1 || ropePositions.Count == 1)
                {
                    Vector3 ropePosition = ropePositions[ropePositions.Count - 1];
                    ropePosition.z = ropeHingeAnchor.transform.position.z;
                    if (ropePositions.Count == 1)
                    {
                        ropeHingeAnchorRb.transform.position = ropePosition;
                        
                        if (!distanceSet)
                        {
                            ropeJoint.distance = Vector2.Distance(transform.position, ropePosition);
                            distanceSet = true;
                        }
                    }
                    else
                    {
                        ropeHingeAnchorRb.transform.position = ropePosition;
                        if (!distanceSet)
                        {
                            ropeJoint.distance = Vector2.Distance(transform.position, ropePosition);
                            distanceSet = true;
                        }
                    }
                }
                else if (i - 1 == ropePositions.IndexOf(ropePositions.Last()))
                {

                    Vector3 ropePosition = ropePositions.Last();
                    ropePosition.z = ropeHingeAnchor.transform.position.z;
                    ropeHingeAnchorRb.transform.position = ropePosition;

                    if (!distanceSet)
                    {
                        ropeJoint.distance = Vector2.Distance(transform.position, ropePosition);
                        distanceSet = true;
                    }
                }
            }
            else
            {

                ropeRenderer.SetPosition(i, ropeArm.position);
            }
        }
    }
    private void HandleRopeUnwrap()
    {
        if (ropePositions.Count <= 1)
        {
            return;
        }
        // Hinge = next point up from the player position
        // Anchor = next point up from the Hinge
        //Hinge Angle = Angle between anchor and hinge
        // Player Angle = Angle between anchor and player

        var anchorIndex = ropePositions.Count - 2;
        var hingeIndex = ropePositions.Count - 1;
        var anchorPosition = ropePositions[anchorIndex];
        var hingePosition = ropePositions[hingeIndex];
        var hingeDir = hingePosition - anchorPosition;
        var hingeAngle = Vector2.Angle(anchorPosition, hingeDir);
        var playerDir = (Vector2)transform.position - anchorPosition;
        var playerAngle = Vector2.Angle(anchorPosition, playerDir);
        if (!wrapPointsLookup.ContainsKey(hingePosition))
        {
            Debug.LogError("We were not tracking hingePosition (" + hingePosition + ") in the look up dictionary.");
            return;
        }
        if (playerAngle < hingeAngle)
        {
            if (wrapPointsLookup[hingePosition] == 1)
            {
                UnwrapRopePosition(anchorIndex, hingeIndex);
                return;
            }
            wrapPointsLookup[hingePosition] = -1;
        }
        else
        {
            if (wrapPointsLookup[hingePosition] == -1)
            {
                UnwrapRopePosition(anchorIndex, hingeIndex);
                return;
            }
            wrapPointsLookup[hingePosition] = 1;
        }

    }
    private void UnwrapRopePosition(int anchorIndex, int hingeIndex)
    {
        Vector3 newAnchorPosition = ropePositions[anchorIndex];
        newAnchorPosition.z = ropeHingeAnchor.transform.position.z;
        wrapPointsLookup.Remove(ropePositions[hingeIndex]);
        ropePositions.RemoveAt(hingeIndex);

        ropeHingeAnchorRb.transform.position = newAnchorPosition;
        distanceSet = false;

        // Set new rope distance joint distance for anchor position if not yet set.
        if (distanceSet)
        {
            return;
        }
        ropeJoint.distance = Vector2.Distance(transform.position, newAnchorPosition);
        distanceSet = true;
        
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, aim * 10);
    }

}
