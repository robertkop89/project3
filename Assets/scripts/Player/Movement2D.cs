﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement2D : MonoBehaviour
{
    private CapsuleCollider2D cc2d;
    [Header("Forces applied")]
    public float speed = 15f;
    public float crossingSpeed = 10f;
    public float swingForce = 5f;
    [Header("Multipliers")]
    public float runningMultiplier = 1.4f;
    public float falling_movement_multiplier = 4f;
    public float standing_jump_height_multiplier = 2f;
    public float standing_jump_distance_multiplier = 1f;
    public float moving_jump_height_multiplier = 3f;
    public float moving_jump_distance_multiplier = 1f;
    public float running_jump_height_multipier=5f;
    public float running_jump_distance_multiplier = 1f;
    public float jumpGravityMultiplier = 1f;
    public float fallingGravityMultiplier = 2f;

    [Header("Jumping")]
    public float jumpSpeed = 3f;
    public float jumpInputTimeGrace = 0.25f;
    float jumpTimer;
    
    
    [Header("States")]
    public bool groundCheck;
    public bool isSwinging;
    public bool moving;
    public bool knockbackFromRight;
    public bool isJumping;
    public bool isAlive;
    [SerializeField]
    private bool isCrouching;
    [Header("Knockback")]
    public float knockback;
    public float knockbackDuration;
    public float knockbackCount;
    public Vector2 knockbackDirection;
    [Header("GroundCheckParameters")]
    public LayerMask groundLayer;
    [SerializeField]
    private float GroundCheckHeight = 0.1f;
    [SerializeField]
    private float fallingGroundCheckHeight = 0.3f;
    [SerializeField]
    private float swingingGroundCheckHeight = 0.5f;
    [SerializeField]
    private float swingingGroundCheckExtraWidth = 0.1f;
    [Header("OtherSettings")]

    public Transform RobotModel;
    public float running_jump_velocity_threshold = 40;
    private SpriteRenderer playerSprite;
    private Rigidbody2D rBody;
    private float horizontalInput;
    public Vector2 ropeHook;
    public float ground_detection_lenght;
    bool running = false;
    Vector3 squat = new Vector3(1, 0.5f, 1);
    bool isCrossing = false;
    float verticalInput;
    float upper_limit = -1;
    float lower_limit = 1;
    Crossin currentCrossing;
    bool crouching;
    float halfHeight;
    
    [SerializeField] Vector3 offset;
    [SerializeField] Animator _anim;
    [SerializeField] playerHealth ph;
    [SerializeField] float deathRebootTime=2;
    private float cc2dSizeY;
    Scene loadedLevel;
    [SerializeField]
    AudioSource movementAudio;
    [SerializeField]
    [Range(0,1)]
    float minMovementVolume=0.3f;
    [SerializeField]
    [Range(0, 1)]
    float maxMovementVolume=0.5f;


    public Animator GetAnimator()
    {
        return _anim;
    }
    public Rigidbody2D GetRigidBody()
    {
        return rBody;
    }
    public void DEATH()
    {
        isAlive = false;
        _anim.SetBool("Death", true);
        Invoke("RestartScene", deathRebootTime);
    }
    private void RestartScene()
    {
        RespawnController.Instance.RespawnPlayer();
        //SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    void Awake()
    {
        loadedLevel = SceneManager.GetActiveScene();
        cc2d = GetComponent<CapsuleCollider2D>();
        cc2dSizeY = cc2d.size.y;
        playerSprite = GetComponent<SpriteRenderer>();
        rBody = GetComponent<Rigidbody2D>();
        halfHeight = cc2d.size.y / 2;
        isAlive = true;
    }
    private bool IsCeling(float extraCheckHeight, float extraWidth = 0)
    {
        Vector2 size = cc2d.bounds.size + new Vector3(extraWidth, 0, 0);
        RaycastHit2D raycastHit = Physics2D.BoxCast(cc2d.bounds.center+Vector3.up*0.1f,
            size,
            0, Vector2.up,
            GroundCheckHeight, groundLayer);
        if (Debug.isDebugBuild)
        {
            Color rayColor;
            if (raycastHit.collider != null)
                rayColor = Color.yellow;
            else
                rayColor = Color.blue;
            Debug.DrawRay(cc2d.bounds.center + new Vector3(cc2d.bounds.extents.x + extraWidth, 0)
                , Vector2.up * (cc2d.bounds.extents.y + extraCheckHeight), rayColor);
            Debug.DrawRay(cc2d.bounds.center - new Vector3(cc2d.bounds.extents.x + extraWidth, 0)
                , Vector2.up * (cc2d.bounds.extents.y + extraCheckHeight), rayColor);
            Debug.DrawRay(
                cc2d.bounds.center -
                new Vector3(
                    cc2d.bounds.extents.x + extraWidth,
                    cc2d.bounds.extents.y + extraCheckHeight
                ),
                Vector2.right * ((cc2d.bounds.extents.x + extraWidth) * 2),
                rayColor
               );
        }
        return raycastHit.collider != null;
    }
    private bool IsGrounded(float extraCheckHeight,float extraWidth=0)
    {
        //Debug.Log(cc2d.bounds.size);
        Vector2 size = cc2d.bounds.size + new Vector3(extraWidth, 0,0);
        RaycastHit2D raycastHit = Physics2D.BoxCast(cc2d.bounds.center, 
            size,
            0, Vector2.down,
            GroundCheckHeight, groundLayer) ;
        if (Debug.isDebugBuild )
        {
            Color rayColor;
            if (raycastHit.collider != null)
                rayColor = Color.green;
            else
                rayColor = Color.red;
            Debug.DrawRay(cc2d.bounds.center + new Vector3(cc2d.bounds.extents.x + extraWidth, 0)
                , Vector2.down * (cc2d.bounds.extents.y + extraCheckHeight), rayColor);
            Debug.DrawRay(cc2d.bounds.center - new Vector3(cc2d.bounds.extents.x + extraWidth, 0)
                , Vector2.down * (cc2d.bounds.extents.y + extraCheckHeight), rayColor);
            Debug.DrawRay(
                cc2d.bounds.center -
                new Vector3(
                    cc2d.bounds.extents.x + extraWidth,
                    cc2d.bounds.extents.y + extraCheckHeight
                ),
                Vector2.right * ((cc2d.bounds.extents.x + extraWidth) * 2),
                rayColor
               );
        }
        //color
        return raycastHit.collider != null;
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            jumpTimer = Time.time + jumpInputTimeGrace;
        }
        horizontalInput = Input.GetAxis("Horizontal");
        //verticalInput = Input.GetAxis("Vertical");
        if (Input.GetButton("Run") || Input.GetAxis("Run")>0)
        {
            running = true;
            //horizontalInput *= runningMultiplier;
        }
        else running = false;
        //groundCheck = Physics2D.Raycast(transform.position + offset, Vector2.down, ground_detection_lenght, groundLayer) || Physics2D.Raycast(transform.position - offset, Vector2.down, ground_detection_lenght, groundLayer);
        if (isSwinging)
        {
            groundCheck = IsGrounded(swingingGroundCheckHeight,swingingGroundCheckExtraWidth);
        }
        else
        {
            if (rBody.velocity.y < 0)
            {
                rBody.gravityScale = fallingGravityMultiplier;
                groundCheck = IsGrounded(fallingGroundCheckHeight, -0.1f);
                if (groundCheck)
                {
                    _anim.SetBool("Landing", true);
                    _anim.SetBool("Apex", false);
                }
            }
            else
            {
                groundCheck = IsGrounded(GroundCheckHeight, -0.1f);
                rBody.gravityScale = jumpGravityMultiplier;
            }
        }
        if(isJumping && groundCheck && rBody.velocity.y<=0)
        {
            _anim.SetBool("Landing", true);
            _anim.SetBool("Apex", false);
            isJumping = false;
            //_anim.ResetTrigger("Jumping");
        }
        
        if (Input.GetButton("Crouch"))
        {
            crouching = true;
        }
        else
            crouching = false;
        if (moving && !isSwinging && !isJumping && !movementAudio.isPlaying)
        {
            movementAudio.volume = Random.Range(minMovementVolume, maxMovementVolume);
            movementAudio.pitch = Random.Range(0.8f, 1.1f);
            movementAudio.Play();
        }
        if (!moving)
            movementAudio.Stop();
        
    }

    void FixedUpdate()
    {
        if (isAlive)
        {
            if (knockbackCount > 0)
            {
                rBody.AddForce(knockbackDirection * knockback * Time.deltaTime, ForceMode2D.Impulse);
                //rBody.velocity = knockbackDirection * knockback * Time.deltaTime;
                knockbackCount -= Time.deltaTime;
                return;
            }
            #region vertical movement
            /*if (isCrossing && verticalInput != 0)
            {
                RobotModel.forward = new Vector3(0, RobotModel.position.y, verticalInput);
                //transform.right = new Vector3(0, 0, verticalInput);
                if (transform.position.z <= upper_limit && verticalInput > 0)
                {
                    transform.position += new Vector3(0, 0, verticalInput * crossingSpeed * Time.deltaTime);
                    if (transform.position.z > upper_limit) currentCrossing.UnlockPath();
                }
                if (transform.position.z >= lower_limit && verticalInput < 0)
                {
                    transform.position += new Vector3(0, 0, verticalInput * crossingSpeed * Time.deltaTime);
                    if (transform.position.z < lower_limit) currentCrossing.UnlockPath();
                }
                //groundCheck = false;
                jumpTimer = 0;
            }
            if (isCrossing && transform.position.z > lower_limit && transform.position.z < upper_limit)
            {
                horizontalInput = 0;
            }*/
            #endregion
            #region horizontal movement
            if (rBody.velocity.y <= 0 && !groundCheck)
            {
                _anim.SetBool("Apex", true);
            }
            #region crouch
            if (crouching)
            {
                isCrouching = true;
                cc2d.size = new Vector2(cc2d.size.x, cc2dSizeY *9 / 12);
                cc2d.offset = new Vector2(0, -cc2dSizeY *4 / 32);
                //transform.localScale = squat;
                _anim.SetBool("Crouch", true);
            }
            else
            {
                if (isCrouching && !IsCeling(cc2dSizeY/2))
                {
                    isCrouching = false;
                    cc2d.size = new Vector2(cc2d.size.x, cc2dSizeY);
                    cc2d.offset = new Vector2(0, 0);
                    //transform.localScale = Vector3.one;
                    _anim.SetBool("Crouch", false);
                }
            }
            #endregion
            if (horizontalInput != 0)
            {
                transform.right = new Vector3(horizontalInput, 0, 0);
                RobotModel.forward = new Vector3(horizontalInput, 10, 0);
                moving = true;
                playerSprite.flipX = horizontalInput < 0f;
                if (isSwinging)
                {
                    moving = false;
                    _anim.SetBool("Running", false);
                    _anim.SetBool("Walking", false);
                    var playerToHookDirection = (ropeHook - (Vector2)transform.position).normalized;
                    Vector2 perpendicularDirection;
                    if (horizontalInput < 0)
                    {
                        perpendicularDirection = new Vector2(-playerToHookDirection.y, playerToHookDirection.x);
                        var leftPerpPos = (Vector2)transform.position - perpendicularDirection * -2f;
                        Debug.DrawLine(transform.position, leftPerpPos, Color.green, 0f);
                        if (playerToHookDirection.x > 0)
                        {
                            _anim.SetFloat("Swing", -1);
                        }
                        else
                        {
                            _anim.SetFloat("Swing", 1);
                        }
                    }
                    else
                    {
                        perpendicularDirection = new Vector2(playerToHookDirection.y, -playerToHookDirection.x);
                        var rightPerpPos = (Vector2)transform.position + perpendicularDirection * 2f;
                        Debug.DrawLine(transform.position, rightPerpPos, Color.green, 0f);
                        if (playerToHookDirection.x < 0)
                        {
                            _anim.SetFloat("Swing", -1);
                        }
                        else
                        {
                            _anim.SetFloat("Swing", 1);
                        }
                    }

                    var force = perpendicularDirection * swingForce;
                    rBody.AddForce(force, ForceMode2D.Force);
                    //rBody.MovePosition((Vector2)transform.position + force);
                }
                else
                {
                    _anim.SetFloat("Swing", 0);
                    if (groundCheck)
                    {
                        if (!isCrouching && running)
                        {
                            _anim.SetBool("Running", true);
                            horizontalInput *= runningMultiplier;
                        }
                        else
                        {
                            _anim.SetBool("Running", false);
                        }
                        var groundForce = speed * 2f;
                        rBody.AddForce(new Vector2((horizontalInput * groundForce - rBody.velocity.x) * groundForce, 0));
                        //rBody.velocity = new Vector2(rBody.velocity.x, rBody.velocity.y);

                    }
                    else
                    {
                        
                        _anim.SetBool("Running", false);
                        if (rBody.velocity.y <= 0)
                        {

                            var groundForce = speed * falling_movement_multiplier;
                            if (Mathf.Abs(rBody.velocity.x) > 25)
                            {
                                if (Mathf.Sign(rBody.velocity.x) != Mathf.Sign(horizontalInput))
                                    rBody.velocity = new Vector2(groundForce * horizontalInput, rBody.velocity.y);
                            }
                            else
                                rBody.AddForce(new Vector2(horizontalInput * groundForce, 0));
                            //rBody.AddForce(new Vector2(horizontalInput *groundForce , 0),ForceMode2D.Force);
                        }
                    }
                }

            }
            else
            {
                if (isSwinging)
                {
                    if(ropeHook.x - transform.position.x < 0)
                    {
                        _anim.SetFloat("Swing", -1);
                    }
                    else
                    {
                        _anim.SetFloat("Swing", 1);
                    }
                }
                else
                    _anim.SetFloat("Swing", 0);
                moving = false;
                _anim.SetBool("Running", false);
                if (groundCheck)
                    rBody.velocity = new Vector2(0, rBody.velocity.y);
            }
            _anim.SetBool("Walking", moving);
            #endregion

            if (!groundCheck || isSwinging) return;
            #region jump
            Jump();
            #endregion
        }

    }

    private void Jump()
    {

        if (jumpTimer > Time.time)
        {
            movementAudio.Stop();
            isJumping = true;
            _anim.SetBool("Running", false);
            _anim.SetBool("Walking", false);
            _anim.SetTrigger("Jumping");
            _anim.SetBool("Apex", false);
            //Debug.Log("Set Trigger?");
            _anim.SetBool("Landing", false);
            // movded to animationevents
            if (moving)
            {

                if (Mathf.Abs(rBody.velocity.x) > running_jump_velocity_threshold)
                {
                    rBody.velocity = new Vector2(rBody.velocity.x * running_jump_distance_multiplier, jumpSpeed * running_jump_height_multipier);
                    //rBody.AddForce(new Vector2(0, jumpSpeed * running_jump_multipier), ForceMode2D.Impulse);
                }
                else
                {
                    rBody.velocity = new Vector2(rBody.velocity.x * moving_jump_distance_multiplier, jumpSpeed * moving_jump_height_multiplier);
                    //rBody.AddForce(new Vector2(0, jumpSpeed * moving_jump_multiplier), ForceMode2D.Impulse);
                }
            }
            else
            {
                //Debug.Log(RobotModel.forward);
                rBody.velocity = new Vector2(0, jumpSpeed * standing_jump_height_multiplier);
                //rBody.AddForce(new Vector2(0, jumpSpeed * standing_jump_multiplier), ForceMode2D.Impulse);
            }
            jumpTimer = 0;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Tags.Enemy))
        {
            ph.TakeDamage(collision.gameObject.transform);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case Tags.Crossing:
            {
                    if (!isCrossing)
                    {
                        currentCrossing = collision.gameObject.GetComponent<Crossin>();
                        upper_limit = currentCrossing.GetUpPosition();
                        lower_limit = currentCrossing.GetDownPosition();

                        isCrossing = true;
                    }
                    break;
            }
            case Tags.Death:
                {
                    ph.TakeDamage(null);
                    break;
                }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag(Tags.Crossing))
        {
            upper_limit = -1;
            lower_limit = 1;
            isCrossing = false;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(transform.position + offset, transform.position + offset + Vector3.down * ground_detection_lenght);
        //Gizmos.DrawLine(transform.position - offset, transform.position - offset + Vector3.down * ground_detection_lenght);

    }
    public void Knockback(Transform target)
    {
        knockbackDirection =  transform.position- target.position ;
        Debug.DrawLine(transform.position, transform.position + (Vector3)knockbackDirection
            , Color.red, 2f);
        knockbackCount = knockbackDuration;
    }
}
