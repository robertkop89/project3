﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swingScript : MonoBehaviour
{
    Rigidbody _rb;
    public float force = 10f;
    Vector3 inputs = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        inputs = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
    }
    private void FixedUpdate()
    {
        if(inputs != Vector3.zero)
        {
            _rb.AddForce(transform.TransformVector(inputs)*force);
        }
    }
}
