﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody myRigidBody;
    Camera mainCamera;
    float dashVelocity;
    Vector3 _inputs;
    Vector3 movement;
    [SerializeField]
    float speed = 20;

    Vector3 lookPos;

    // Start is called before the first frame update
    void Start()
    {
        
        myRigidBody = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 100))
        {
            lookPos = hit.point;
        }
        Vector3 lookDir = lookPos - transform.position;
        lookDir.y = 0;
        transform.LookAt(transform.position + lookDir, Vector3.up);
        movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

    }
    private void FixedUpdate()
    {
        myRigidBody.MovePosition(transform.position + movement * speed * Time.deltaTime);
    }
    



    private void OnDrawGizmos()
    {
        
    }
}

