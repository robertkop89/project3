﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemBase : MonoBehaviour, IInventoryItem
{
    public Inventory inventory;
    public Sprite _Image;
    public Sprite Image
    {
        get
        {
            return _Image;
        }
    }

    public virtual string Name
    {
        get
        {
            return "_name_";
        }
    }

    public void OnPickup()
    {
        // logic for pickup by player
        gameObject.SetActive(false);
    }
    public bool OnDrop()
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit, 1000))
        {
            gameObject.SetActive(true);
            gameObject.transform.position = hit.point;
            return true;
        }
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        inventory.AddItem(this);
    }
}
