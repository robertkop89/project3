﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDropHandler : MonoBehaviour, IDropHandler
{
    public Inventory _Inventory;
    public void OnDrop(PointerEventData eventData)
    {
        RectTransform invPnael = transform as RectTransform;
        if (!RectTransformUtility.RectangleContainsScreenPoint(invPnael, Input.mousePosition)){
            IInventoryItem item = eventData.pointerDrag.gameObject.GetComponent<ItemDragHandler>().Item;
            if (item != null)
            {
                if(item.OnDrop()) _Inventory.RemoveItem(item); 
            }
        }

    }

}
