﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableBox : MonoBehaviour
{
    public Vector3 lastPos;
    public bool beingPushed;
    float xPos;
    [SerializeField]
    AudioSource dragSound;
    // Start is called before the first frame update
    void Start()
    {
        xPos = transform.position.x;
        lastPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (beingPushed == false)
        {
            this.gameObject.isStatic = true;
            transform.position = new Vector3(xPos, transform.position.y);
            dragSound.Stop();

        }
        else
        {
            this.gameObject.isStatic = false;
            if (!dragSound.isPlaying)
                dragSound.Play();
            
        }

            xPos = transform.position.x;
            
    }
}
