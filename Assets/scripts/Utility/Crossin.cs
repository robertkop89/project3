﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Crossin : MonoBehaviour
{
    float UpPosition;
    float DownPosition;
    static GameObject player;
    float upperBound, lowerBound;
    // Start is called before the first frame update
    public List<GameObject> upPath;
    public List<GameObject> downPath;
    public List<MouseHoleScript> upperSpawns;
    public List<MouseHoleScript> lowerSpawns;
    public List<IEnemy> upperEnemies;
    public List<IEnemy> lowerEnemies;
    //public List<GameObject> other;

    private void Awake()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }
    void Start()
    {
        UpPosition = transform.GetChild(0).transform.position.z;
        DownPosition = transform.GetChild(1).transform.position.z;
        upperBound = transform.position.z + transform.localScale.z / 2;
        lowerBound = transform.position.z - transform.localScale.z / 2;
        //GameObject[] roads = ;
        AssignGround(GameObject.FindGameObjectsWithTag("Ground"));
        AssignGround(GameObject.FindGameObjectsWithTag("hook"));
        AssignSpawns();
        //OtherPath();
        UnlockPath();
    }
    private void AssignGround(GameObject[] roads)
    {
        foreach (GameObject g in roads)
        {
            if (InUpperBound(g.transform.position.z))
            {
                upPath.Add(g);
            }
            else if (InLowerBound(g.transform.position.z))
            {
                downPath.Add(g);
            }/*
            else
            {
                other.Add(g);
            }*/
        }
    }
    private void AssignSpawns()
    {
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("MouseHole"))
        {
            if (InUpperBound(g.transform.position.z))
            {
                MouseHoleScript hole = g.GetComponent<MouseHoleScript>();
                if(hole!=null)
                    upperSpawns.Add(hole);
            }
            else if (InLowerBound(g.transform.position.z))
            {
                MouseHoleScript hole = g.GetComponent<MouseHoleScript>();
                if (hole != null)
                    lowerSpawns.Add(hole);
            }
        }
    }
    private void AssignEnemies()
    {
        upperEnemies = new List<IEnemy>();
        lowerEnemies = new List<IEnemy>();
        foreach (GameObject e in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (InUpperBound(e.transform.position.z))
            {
                IEnemy enemy = e.GetComponent<IEnemy>();
                if (enemy != null)
                    upperEnemies.Add(enemy);
            }
            else if (InLowerBound(e.transform.position.z))
            {
                IEnemy enemy = e.GetComponent<IEnemy>();
                if (enemy != null)
                    lowerEnemies.Add(enemy);
            }
        }
    }

    bool InUpperBound(float target)
    {
        return (target > UpPosition && target < upperBound);
    }

    bool InLowerBound(float target)
    {
        return (target < DownPosition && target > lowerBound);
    }

    public float GetUpPosition()
    {
        return UpPosition;
    }
    public float GetDownPosition()
    {
        return DownPosition;
    }
    public void UnlockPath()
    {
        AssignEnemies();
        UppperPath();
        LowerPath();
    }
    /*private void OtherPath()
    {
        foreach(GameObject ground in other)
        {
            ground.GetComponent<Collider2D>().enabled = false;
        }
    }*/
    private void UppperPath()
    {
        if (InUpperBound(player.transform.position.z))
        {
            foreach (GameObject ground in upPath)
            {
                switch (ground.tag)
                {
                    case "hook":
                        Collider2D[] temp = ground.gameObject.GetComponentsInChildren<Collider2D>();
                        foreach (Collider2D t in temp) t.enabled = true;
                        break;
                    default:
                        ground.GetComponent<Collider2D>().enabled = true;
                        break;
                }
            }
            foreach(MouseHoleScript hole in upperSpawns)
            {
                hole.Live();
                //Debug.Log("Live");
            }
            foreach(IEnemy enemy in upperEnemies)
            {
                enemy.Alive();
            }
        }
        else
        {
            foreach (GameObject ground in upPath)
            {
                switch (ground.tag)
                {
                    case "hook":
                        Collider2D[] temp = ground.gameObject.GetComponentsInChildren<Collider2D>();
                        foreach (Collider2D t in temp) t.enabled = false;
                        break;
                    default:
                        ground.GetComponent<Collider2D>().enabled = false;
                        break;
                }
                
            }
            foreach (MouseHoleScript hole in upperSpawns)
            {
                hole.Suspend();
            }
            foreach(IEnemy enemy in upperEnemies)
            {
                enemy.Limbo();
            }
        }
    }
    private void LowerPath()
    {
        if (InLowerBound(player.transform.position.z))
        {
            foreach (GameObject ground in downPath)
            {
                switch (ground.tag)
                {
                    case "hook":
                        Collider2D[] temp = ground.gameObject.GetComponentsInChildren<Collider2D>();
                        foreach (Collider2D t in temp) t.enabled = true;
                        break;
                    default:
                        ground.GetComponent<Collider2D>().enabled = true;
                        break;
                }
            }
            foreach (MouseHoleScript hole in lowerSpawns)
            {
                hole.Live();
            }
            foreach (IEnemy enemy in lowerEnemies)
            {
                enemy.Alive();
            }
        }
        else
        {
            foreach (GameObject ground in downPath)
            {
                switch (ground.tag)
                {
                    case "hook":
                        Collider2D[] temp = ground.gameObject.GetComponentsInChildren<Collider2D>();
                        foreach (Collider2D t in temp) t.enabled = false;
                        break;
                    default:
                        ground.GetComponent<Collider2D>().enabled = false;
                        break;
                }
            }
            foreach (MouseHoleScript hole in lowerSpawns)
            {
                hole.Suspend();
            }
            foreach (IEnemy enemy in lowerEnemies)
            {
                enemy.Limbo();
            }
        }
    }
    private void OnDrawGizmos()
    {
        
        Vector3 upperBound=transform.position, lowerBound=transform.position,UpPos=transform.position,DownPos=transform.position;
        upperBound.z = upperBound.z + transform.localScale.z / 2;
        lowerBound.z = lowerBound.z - transform.localScale.z / 2;
        UpPos.z = UpPosition;
        DownPos.z = DownPosition;
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(UpPos + Vector3.up * 5, UpPos+Vector3.down*5);
        Gizmos.DrawLine(upperBound+Vector3.up*5, upperBound+ Vector3.down * 5);
        //Handles.Label(UpPos + Vector3.up * 4, UpPos.z.ToString());
       // Handles.Label(upperBound + Vector3.up * 4, upperBound.z.ToString());
        Gizmos.color = Color.green;
        Gizmos.DrawLine(lowerBound + Vector3.up * 5, lowerBound + Vector3.down * 5);
        Gizmos.DrawLine(DownPos + Vector3.up * 5, DownPos+Vector3.down*5);
        //Handles.Label(DownPos + Vector3.up * 4, DownPos.z.ToString());
        //Handles.Label(lowerBound + Vector3.up*4, lowerBound.z.ToString());

    }
}
