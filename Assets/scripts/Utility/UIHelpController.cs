﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHelpController : MonoBehaviour
{
    [SerializeField]
    private GameObject  helpText;
    [SerializeField]
    private Button exit;
    [SerializeField]
    private Image buttonImage;
    [SerializeField]
    private Text paused;
    // Start is called before the first frame update
    void Start()
    {
        if (helpText != null)
        {
            paused.enabled = false;
            helpText.SetActive(false);
            buttonImage.enabled = false;
            exit.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Help"))
        {
            helpText.SetActive(!helpText.activeSelf);
            if (helpText.activeSelf)
            {
                paused.enabled = true;
                exit.enabled = true;
                buttonImage.enabled = true;
                Time.timeScale = 0;
            }
            else
            {
                paused.enabled = false;
                buttonImage.enabled = false;
                exit.enabled = false;
                Time.timeScale = 1f;
            }
        }
    }
}
