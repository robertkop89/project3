﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public float speed = 10;
    private Vector3 off;

    // Start is called before the first frame update
    void Start()
    {   
        player = GameObject.FindGameObjectWithTag("Player").transform;
        off = transform.position - player.transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = Vector3.Lerp(transform.position, player.position + off, Time.deltaTime * speed);
        transform.position = newPosition;
    }
}
