﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Image foregroundImage;
    [SerializeField]
    private float updateSpeedSeconds= 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInParent<IEnemy>().OnHealthChange += HealthChange;
    }

    private void HealthChange(float pct)
    {
        StartCoroutine(ChangeTo(pct));
    }

    private IEnumerator ChangeTo(float pct)
    {
        float preChange = foregroundImage.fillAmount;
        float elapsed = 0f;
        while (elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChange, pct, elapsed / updateSpeedSeconds);
            yield return null;
        }
        foregroundImage.fillAmount = pct;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
