﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject CreditsPanel;
    [SerializeField] GameObject MainMenuPanel;

    private void Start()
    {
        if(MainMenuPanel!=null)
        MainMenuPanel.SetActive(true);
        if(CreditsPanel!=null)
        CreditsPanel.SetActive(false);
    }
    public void ShowMain()
    {
        MainMenuPanel?.SetActive(true);
        CreditsPanel?.SetActive(false);
    }
    public void Credits()
    {
        MainMenuPanel?.SetActive(false);
        CreditsPanel?.SetActive(true);
    }
    public void StartGame()
    {
        SceneManager.LoadScene("First Level");
    }
    public void Exit()
    {
        Debug.Log("ARGH!!!");
        Application.Quit();
    }
    public void Menu()
    {
        //Debug.Log("Menu?");
        SceneManager.LoadScene("mainMenu");
    }
    public void GameOver()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("mainMenu");
    }
}
