﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public interface IEnemy
{
    void TakeDamage();
    void TakeDamage(int i);
    void Alive();
    void Limbo();
    event Action<float> OnHealthChange;
}
