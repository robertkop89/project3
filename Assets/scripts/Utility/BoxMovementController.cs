﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMovementController : MonoBehaviour
{
    Rigidbody2D _rb;
    [SerializeField]
    float grabDistance = 1f;
    [SerializeField]
    LayerMask boxMask;
    GameObject box;
    FixedJoint2D fixJ;
    PushableBox pushBox;

    // Start is called before the first frame update
    void Start()
    {
        _rb=this.GetComponentInParent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right*transform.localScale.x,grabDistance,boxMask);
        if(hit.collider!=null && hit.collider.gameObject.tag=="pushable" && Input.GetButtonDown("Interact"))
        {
            box = hit.collider.gameObject;
            fixJ = box.GetComponent<FixedJoint2D>();
            fixJ.connectedBody = _rb;
            fixJ.enabled = true;
            pushBox = box.GetComponent<PushableBox>();
            pushBox.beingPushed = true;
        }else if (Input.GetButtonUp("Interact"))
        {
            if(fixJ!=null)
            fixJ.enabled = false;
            if(pushBox!=null)
            pushBox.beingPushed = false;
        }
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + transform.right * transform.localScale.x * grabDistance);
    }
}
