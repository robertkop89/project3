﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController :MonoBehaviour
{
    private static MusicController _instance;
    public static MusicController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (MusicController)FindObjectOfType(typeof(MusicController));
                if(_instance == null)
                {
                    _instance = new GameObject().AddComponent<MusicController>();
                }
            }
            return _instance;
        }
    }
    [SerializeField]
    AudioClip chillClip;
    [SerializeField]
    AudioClip battleClip;
    [SerializeField]
    AudioSource playerAudioSource;
    private int activeCounter=0;

    private void Awake()
    {
        if (playerAudioSource == null)
        {
            playerAudioSource=
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        }
    }
    void Start()
    {
        playerAudioSource.clip = chillClip;
        playerAudioSource.Play();
    }
    public void TestMethod()
    {
        Debug.Log("This is a test");
    }
    public void Action(int i = 1)
    {
        if (i > 1) activeCounter += i;
        //Debug.Log("Action Call");
        if (activeCounter++ >= 0)
        {
            playerAudioSource.clip = battleClip;
            playerAudioSource.Play();
        }
    }
    public void Release(int i=1)
    {
        if (i > 1) activeCounter = 1;
        //Debug.Log("Realse Call");
        if (--activeCounter == 0)
        {
            playerAudioSource.clip = chillClip;
            playerAudioSource.Play();
        }
    }
}
