﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags 
{
    public const string Player = "Player";
    public const string Crossing = "Crossing";
    public const string Hook = "hook";
    public const string Ground = "Ground";
    public const string Bullet = "Bullet";
    public const string TieBullet = "TieBuller";
    public const string Enemy = "Enemy";
    public const string MouseHole = "MouseHole";
    public const string Death = "Death";
    public const string pushable = "pushable";
}
