﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemyOnContact : MonoBehaviour
{
    public int StompDamage;
    public float bounceSpeed;

    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = transform.parent.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Enemy) && rb.velocity.y<0)
        {
            collision.gameObject.GetComponent<IEnemy>().TakeDamage();
            rb.velocity = new Vector2(rb.velocity.x, bounceSpeed);
        }
    }


}
