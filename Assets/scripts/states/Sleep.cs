﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sleep : State
{
    public Sleep(GameObject _npc, Transform _player, Vector3 _sp, Animator _anim) :
        base(_npc, _player, _sp, _anim)
    {
        name = STATE.IDLE;
    }
    public override void Enter()
    {
        anim.SetTrigger("isIdle");
        base.Enter();
    }
    
    public override void Exit()
    {
        anim.ResetTrigger("isIdle");
        base.Exit();
    }
}
