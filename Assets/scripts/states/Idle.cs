﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : State
{
    BearAi _ai;
    bool bigBear;
   public Idle(GameObject _npc, Transform _player,Vector3 _sp, Animator _anim) :
        base(_npc, _player,_sp,_anim)
    {
        name = STATE.IDLE;
        _ai = npc.GetComponent<BearAi>();
        BigBear test = npc.GetComponent<BigBear>();
        if (test != null) bigBear = true;
    }
    public override void Enter()
    {
        bool action = _ai.action;
        if (action)
        {
            MusicController.Instance.Release();
        }
        _ai.action = false;
        //Debug.Log("Bear Start Idle");
        
        anim.SetTrigger("isIdle");
        anim.SetFloat("speed", 0);
        base.Enter();
    }
    public override void Update()
    {
        if (CanSeePlayer())
        {
            _ai.bearRoar.Play();
           nextState = new Persue(npc, player,sp,anim);
            stage = EVENT.EXIT;
        }
    }
    public override void Exit()
    {
        _ai.action = true;
        
        if(bigBear)
            MusicController.Instance.Action(1);
        else 
            MusicController.Instance.Action();
        anim.ResetTrigger("isIdle");
        base.Exit();
    }
}
