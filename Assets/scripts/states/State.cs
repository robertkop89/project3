﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class State
{
    public enum STATE
    {
        IDLE, PURSUE,ATTACK,TIED,RETURN,TOPPLED,SLEEP
    };
    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };
    public STATE name;
    protected EVENT stage;
    protected GameObject npc;
    protected Transform player;
    protected State nextState;
    protected Vector3 sp;
    protected Animator anim;
    //protected NavMeshAgent agent;
    float visDist=20;
    float attackDist = 4;

    public State(GameObject _npc,Transform _player,Vector3 _sp, Animator _anim) //NavMeshAgent _agent
    {
        npc = _npc;
        player = _player;
        stage = EVENT.ENTER;
        sp = _sp;
        //agent = _agent;
        anim = _anim;
    }
    public virtual void Enter() { stage = EVENT.UPDATE; }
    public virtual void Update() { stage = EVENT.UPDATE; }
    public virtual void Exit() { stage = EVENT.EXIT; }
    public State Process()
    {
       switch (stage)
        {
            case EVENT.ENTER:
                Enter();
                break;
            case EVENT.UPDATE:
                Update();
                break;
            case EVENT.EXIT:
                Exit();
                return nextState;
        }
        return this;
    }
    public bool CanSeePlayer()
    {
        Vector3 direction = player.position - npc.transform.position;
        //float angle = Vector3.Angle(direction, npc.transform.forward);
        //if (direction.magnitude < visDist && angle < visAngle) return true;
        if (direction.magnitude < visDist) return true;
        return false;
    }
    public bool CanAttackPlayer()
    {
        float x = Mathf.Abs( player.position.x - npc.transform.position.x);
        if (x < attackDist)
        {
            return true;
        }
        return false;
    }
    public void TiedByPlayer()
    {
        nextState = new Tied(npc, player,sp,anim);
        stage = EVENT.EXIT;
    }
    public void ToppledOver()
    {
        nextState = new Toppled(npc,player,sp,anim);
        stage = EVENT.EXIT;
    }
    public void Sleep()
    {
        nextState = new Sleep(npc, player, sp,anim);
        stage = EVENT.EXIT;
    }
    public void Awaken()
    {
        nextState = new Idle(npc, player, sp,anim);
        stage = EVENT.EXIT;
    }
}
