﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Persue : State
{
    float speed = 5f;
    BearAi _ai;
    public Persue(GameObject _npc, Transform _player,Vector3 _sp, Animator _anim) :
        base(_npc, _player,_sp,_anim)
    {
        name = STATE.PURSUE;
    }
    public override void Enter()
    {
        anim.SetTrigger("isRunning");
        anim.SetFloat("speed", 1);
        _ai= npc.GetComponent<BearAi>();
        base.Enter();
    }
    public override void Update()
    {
        npc.gameObject.transform.right = new Vector3(player.position.x - npc.gameObject.transform.position.x , 0, 0);
        npc.transform.position = new Vector3(npc.transform.position.x+npc.transform.right.x * Time.deltaTime * speed, npc.transform.position.y, npc.transform.position.z);
        if (!_ai.bearStep.isPlaying)
        {
            _ai.bearStep.volume = Random.Range(_ai.minBearStepVolume, _ai.maxBearStepVolume);
            _ai.bearStep.pitch = Random.Range(0.6f, 1f);
            _ai.bearRoar.Play();
        }
        if (CanAttackPlayer())
        {
            _ai.bearAttack.Play();
            nextState = new Attack(npc, player,sp,anim);
            stage = EVENT.EXIT;
        }
        if (!CanSeePlayer())
        {
            nextState = new Idle(npc, player, sp, anim);
            stage = EVENT.EXIT;
        }
    }
    public override void Exit()
    {
        anim.SetFloat("speed", 0);
        anim.ResetTrigger("isRunning");
        base.Exit();
    }
}
