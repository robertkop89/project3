﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tied : State
{
    float tiedTimer = 0;
    float tiedCooldown=2f;
    public Tied(GameObject _npc,Transform _player,Vector3 _sp, Animator _anim)
        :base(_npc,_player,_sp,_anim)
    {
        name = STATE.TIED;
        tiedTimer = Time.time + tiedCooldown;
    }

    public override void Enter()
    {
        anim.SetTrigger("isTied");
        base.Enter();
    }
    public override void Update()
    {
        if (tiedTimer < Time.time)
        {
            nextState = new Idle(npc, player,sp,anim);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        anim.ResetTrigger("isTied");
        base.Exit();
    }
}
