﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Attack : State
{
    float attackTimer = 0;
    float attackCooldown = 2f;
    //Vector3 attackPosition;
    Rigidbody2D rb;
    RigidbodyType2D oldbody;
    public Attack(GameObject _npc,Transform _player,Vector3 _sp, Animator _anim)
        :base(_npc,_player,_sp,_anim)
    {
        name = STATE.ATTACK;
        
        //attackPosition.position = npc.transform.position + new Vector3(3, 1, 0);
        //attackPosition.rotation = npc.transform.rotation;

    }
    public override void Enter()
    {
        anim.SetFloat("speed", 0);
        anim.SetTrigger("isAttacking");
        npc.GetComponent<BearAi>();
        rb = npc.GetComponent<Rigidbody2D>();
        oldbody = rb.bodyType;
        rb.bodyType = RigidbodyType2D.Kinematic;
        base.Enter();
    }
    public override void Update()
    {
        if (attackTimer < Time.time)
        {
            //anim.ResetTrigger("isAttacking");
            
            attackTimer = Time.time + attackCooldown;
        }

        if (!CanAttackPlayer())
        {
            if (CanSeePlayer()) nextState = new Persue(npc, player,sp,anim);
            else nextState = new Idle(npc, player,sp,anim);
            stage = EVENT.EXIT;
        }
    }
    public override void Exit()
    {
        anim.ResetTrigger("isAttacking");
        rb.bodyType = oldbody;
        base.Exit();
    }
    
}
