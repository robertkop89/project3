﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TestTarget : MonoBehaviour,IEnemy
{
    Renderer ren;
    Color startMat;
    public float pullForce = 15f;
    public float untieTime = 2.0f;
    public float socialDistanceFromTarget = 1.5f;
    bool tied=false;
    public int maxHealth = 2;
    private int currentHealth;
    GameObject targetPlace;

    public event Action<float> OnHealthChange=delegate { };

    // Start is called before the first frame update
    void Start()
    {
        ren = GetComponent<Renderer>();
        startMat= ren.material.color;
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (tied)
        {
            float distance = transform.position.x - targetPlace.transform.position.x;
            if (distance > socialDistanceFromTarget)
            {
                
               transform.position -= transform.right*Time.deltaTime*pullForce;
            }
            else if (distance < -socialDistanceFromTarget)
            {
               transform.position += transform.right * Time.deltaTime * pullForce;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Trigger by" + collision.gameObject.tag);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        //Debug.Log("Triggered by " + collision.gameObject.tag);
        
        if (collision.gameObject.CompareTag(Tags.TieBullet) && !tied)
        {
            ren.material.SetColor("_Color", Color.black);
            tied = true;
            targetPlace = collision.gameObject.GetComponent<TieBullet>().PullLocation;
            Invoke("UnTie", untieTime);

        }
        else
        if (collision.gameObject.CompareTag(Tags.Bullet))
        {
            ren.material.SetColor("_Color", Color.yellow);
            TakeDamage(1);
        }
        else
        if (collision.gameObject.CompareTag(Tags.Player))
        {
            if (collision.gameObject.GetComponent<Movement2D>().isSwinging)
            {
                ren.material.SetColor("_Color", Color.blue);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }
    void UnTie()
    {
        tied = false;
        ren.material.SetColor("_Color",startMat);
    }
    public void TakeDamage()
    {

    }
    public void TakeDamage(int i)
    {
        --currentHealth;
        OnHealthChange((float)currentHealth / (float)maxHealth);
        if (currentHealth <= 0)
        {
            Limbo();
            CancelInvoke();
            Destroy(this.gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        //Handles.Label(transform.position + transform.up * 2, health.ToString());
    }

    public void Alive()
    {
        Collider2D temp=GetComponent<Collider2D>();
        if (temp != null)
        {
            temp.enabled = true;
        }
    }

    public void Limbo()
    {
        Collider2D temp = GetComponent<Collider2D>();
        if (temp != null)
        {
            temp.enabled = false;
        }
    }
}
