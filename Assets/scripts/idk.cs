﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class idk : MonoBehaviour
{
    Animator ani;
    // Start is called before the first frame update
    void Start()
    {
        ani = this.GetComponent<Animator>();
        ani.Play("Armature|3wait");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Vertical") > 0){
            ani.SetTrigger("isWalking");
        }
        else
        {
            ani.ResetTrigger("isWalking");
            ani.SetTrigger("isIdle");
        }
        /*
        if (Input.GetButton("Fire1"))
            ani.SetTrigger("isFighting");
        else
            ani.ResetTrigger("isFighting");
        if (Input.GetButton("Fire2"))
            ani.SetTrigger("isLaud");
        else
            ani.ResetTrigger("isLaud");
        */
    }
}
