﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TieBullet : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float pullSpeed = 50f;
    public GameObject PullLocation;
    Rigidbody2D rb;
    bool movement = true;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(movement)
        transform.position += transform.right * Time.deltaTime * speed;
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!movement)
        {
            return;
        }
        if (collision.CompareTag(Tags.Hook))
        {
            movement = false;
            //collision.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }
}
