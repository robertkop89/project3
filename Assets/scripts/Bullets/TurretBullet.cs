﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    public float speed=35;
    [SerializeField]
    private float lifeTime;
    [SerializeField]
    AudioSource boomSound;
    bool alive = true;
    [SerializeField]
    CircleCollider2D _c2d;
    [SerializeField]
    GameObject _model;
   
    // Start is called before the first frame update
    void Start()
    {
        if (lifeTime > 0)
            Invoke("Die", lifeTime);
        else
            Invoke("Die", 1);
    }

    // Update is called once per frame
    void Die()
    {
        CancelInvoke();
        alive = false;
        _c2d.enabled = false;
        _model.SetActive(false);
        Destroy(this.gameObject,1);
    }
    void Update()
    {
        if(alive)
        transform.position += transform.forward*Time.deltaTime*speed;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag(Tags.Player))
        {
            boomSound.Play();
            collision.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
        }
        Die();
    }
}
