﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Bullet2D: MonoBehaviour
{
    [SerializeField] float speed = 10f;
    bool moving = true;
    [SerializeField]
    float lifetime=2;
    private void Start()
    {
        
        Invoke("Die", lifetime);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(moving)
            transform.position+= transform.right * Time.deltaTime * speed;
    }
    void Die()
    {
        CancelInvoke();
        Destroy(this.gameObject);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {//don't forget a rigidbody
        //Debug.Log("Trigger Enter");
        if (!moving)
        {
            return;
        }
        if(collision.tag!= "hook")
        {
            moving = false;
            //collision.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            Die();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        IEnemy temp = collision.gameObject.GetComponent<IEnemy>();
        if (temp != null)
        {
            ClownAI clown =collision.gameObject.GetComponent<ClownAI>();
            if (clown != null)
                if (collision.transform.position.x < transform.position.x)
                    clown.TakeDamage(1, false);
                else
                    clown.TakeDamage(1, true);
            else
                temp.TakeDamage(1);
        }
        Die();
    }
}
