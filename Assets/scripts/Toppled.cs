﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toppled : State
{
    float toppledTimer = 0;
    float toopledCooldown = 5f;
    public Toppled(GameObject _npc, Transform _player, Vector3 _sp, Animator _anim)
        : base(_npc, _player, _sp,_anim)
    {
        name = STATE.TOPPLED;
    }

        public override void Enter()
    {
        anim.SetTrigger("isToppled");
        this.npc.transform.rotation = Quaternion.Euler(0, this.npc.transform.rotation.eulerAngles.y, 90);
        toppledTimer = Time.time + toopledCooldown;
        base.Enter();
    }
    public override void Update()
    {
        if (toppledTimer < Time.time)
        {
            nextState = new Idle(npc, player, sp,anim);
            stage = EVENT.EXIT;
        }
    }
    public override void Exit()
    {
        anim.ResetTrigger("isTooppled");
        this.npc.transform.rotation = Quaternion.Euler(0, this.npc.transform.rotation.eulerAngles.y, 0);
        base.Exit();
    }
}

