﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTurretAI : MonoBehaviour, IEnemy
{
    private static Transform player;
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private GameObject bulletPrefab;
    private float fireTimer = 0;
    [SerializeField]
    private float fireCooldown;
    public int maxHealth = 2;
    private int currentHealth;
    public bool alive;
    Rigidbody2D rb;
    Collider2D c2d;
    [SerializeField]
    private float detectionDistance = 10f;
    bool action;
    [SerializeField]
    AudioSource turretFire;
    [SerializeField]
    AudioSource turretDeath;
    

    public event Action<float> OnHealthChange = delegate { };

    public void Alive()
    {
        rb.gravityScale = 1000;
        //Debug.Log("alive");
        c2d.enabled = true;
        alive = true;
    }

    public void Limbo()
    {
        rb.gravityScale = 0;
        c2d.enabled = false;
        alive = false;
    }

    public void TakeDamage()
    {
    }

    public void TakeDamage(int i)
    {
        currentHealth -= 1;
        OnHealthChange((float)currentHealth / (float)maxHealth);
        if (currentHealth <= 0)
        {
            Limbo();
            turretDeath.Play();
            if (action)
            {
                action = false;
                MusicController.Instance.Release();
            }
            Destroy(this.gameObject,1);
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        currentHealth = maxHealth;
        rb = GetComponent<Rigidbody2D>();
        c2d = GetComponent<CapsuleCollider2D>();
        if (player is null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (alive)
        {
            transform.right = new Vector3(player.position.x - transform.position.x, 0, 0);
            if ((Mathf.Abs(transform.position.x - player.transform.position.x) < detectionDistance)
                )
            {
                if (!action)
                {
                    action = true;
                    MusicController.Instance.Action();
                }
                if (fireTimer < Time.time)

                {
                    turretFire.Play();
                    Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                    fireTimer = Time.time + fireCooldown;
                }
            }
            else if (action)
            {
                action = false;
                MusicController.Instance.Release();
                
            }
        }
    }
}
