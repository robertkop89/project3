﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAi : MonoBehaviour,IEnemy
{
    Rigidbody2D rb;
    Collider2D c2d;
    public int maxHealth = 2;
    private int currentHealth;
    private static Transform player;
    [Header("TurretComponents")]
    [SerializeField] private Transform turretBase;
    [SerializeField] private Transform turretBarrelPivot;

    [Header("Elevation")]
    [Tooltip("Speed at whihc the turret gubn elevate up and down")]
    public float ElevationSpeed = 30f;

    [Tooltip("Highest upwards elevation")]
    public float maxElevation = 60f;
    [Tooltip("Lowest gun depresion value")]
    public float minElevation = -10f;
    public Vector3 AimPosition = Vector3.zero;
    [SerializeField] private float aimedThreshold = 5f;
    bool isIdle = false;
    private bool IsTurretAtRest;
    private bool isAimed = false;
    private bool isBarrelAtRest;
    private float elevation = 0f;
    public bool DrawDebugRay = true;
    private float angleToTarget;
    private bool active = true;
    [SerializeField]
    private float detectionRange = 5f;
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private GameObject bulletPrefab;
    private float fireTimer=0;
    [SerializeField]
    private float fireDelay;
    bool action;

    public event Action<float> OnHealthChange = delegate { };

    public void Alive()
    {
        c2d.enabled = true;
        rb.gravityScale = 1;
        active = true;
        isIdle = false;
    }

    public void Limbo()
    {
        rb.gravityScale = 0;
        c2d.enabled = false;
        active = false;
        isIdle = true;
    }

    public void TakeDamage()
    {
        
    }

    public void TakeDamage(int i)
    {
        currentHealth -= i;
        OnHealthChange((float)currentHealth/(float)maxHealth);
        if (currentHealth <= 0)
        {
            if(action)
            //MusicController.Instance.Release();
            Limbo();
            Destroy(this.gameObject,1);
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        currentHealth = maxHealth;
        rb = GetComponent<Rigidbody2D>();
        c2d = GetComponent<CapsuleCollider2D>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //rb.AddForce(Vector2.down);
        if (active)
        {
            if (Mathf.Abs(player.transform.position.x - transform.position.x) < detectionRange)
            {
                isIdle = false;
                action = true;
                //MusicController.Instance.Action();
            }else if (!isIdle)
            {
                isIdle = true;
                action = false;
                //MusicController.Instance.Release();
            }
            if (isIdle)
            {
                if (!IsTurretAtRest)
                {
                    RotateBarrelToIdle();
                }
                isAimed = false;
            }
            else
            {
                AimPosition = player.transform.position;
                RotateBarrelToTarget(AimPosition);
                angleToTarget = GetTurretAngleToTarget(AimPosition);
                isAimed = angleToTarget < aimedThreshold;
                if (isAimed && fireTimer < Time.time)
                {
                    Instantiate(bulletPrefab, firePoint, firePoint);
                    fireTimer = Time.time + fireDelay;
                }
                isBarrelAtRest = false;
            }
        }
    }

    private float GetTurretAngleToTarget(Vector3 aimPosition)
    {
        return Vector3.Angle(aimPosition - turretBarrelPivot.position, turretBarrelPivot.forward);
    }

    private void RotateBarrelToTarget(Vector3 aimPosition)
    {
        Vector3 localTargetPos = turretBase.InverseTransformDirection(aimPosition - turretBarrelPivot.position);
        Vector3 flattenedVecForBarrel = Vector3.ProjectOnPlane(localTargetPos, Vector3.up);

        float targetElevation = Vector3.Angle(flattenedVecForBarrel, localTargetPos);
        targetElevation *= Mathf.Sign(localTargetPos.y);
        targetElevation = Mathf.Clamp(targetElevation, minElevation, maxElevation);
        elevation = Mathf.MoveTowards(elevation, targetElevation, ElevationSpeed * Time.deltaTime);
        if (Mathf.Abs(elevation) > Mathf.Epsilon)
            turretBarrelPivot.localEulerAngles = Vector3.right * -elevation;
#if UNITY_EDITOR
        if (DrawDebugRay)
            Debug.DrawRay(turretBarrelPivot.position, turretBarrelPivot.forward * localTargetPos.magnitude, Color.red);
#endif

    }

    private void RotateBarrelToIdle()
    {
        elevation = Mathf.MoveTowards(elevation, 0f, ElevationSpeed * Time.deltaTime);
        if (Mathf.Abs(elevation) > Mathf.Epsilon)
            turretBarrelPivot.localEulerAngles = Vector3.right * elevation;
        else
            isBarrelAtRest = true;
    }
}
