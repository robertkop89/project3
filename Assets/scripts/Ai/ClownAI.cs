﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownAI : MonoBehaviour, IEnemy
{
    bool grounded;
    bool isJumping;
    bool jumpRight;
    bool working;
    private Rigidbody2D rb;
    private Collider2D c2d;
    private static Transform player;
    [SerializeField]
    private LayerMask ground;
    [SerializeField]
    private float ground_detection_distance;
    [SerializeField]
    private float horizontal_force;
    [SerializeField]
    private float up_force;
    [SerializeField]
    private int maxhealth = 3;
    private int currentHealth;
    private float jumpTimer;
    [SerializeField]
    private float jumpMinCooldown=3;
    [SerializeField]
    private float jumpMaxCooldown=4;
    [SerializeField]
    Animator _Anim;
    float turnTimer;
    [SerializeField]
    AudioSource boinkSource;

    public event Action<float> OnHealthChange =delegate { };

    //normal gravity is (0,-9.81)

    // Start is called before the first frame update
    void Awake()
        
    {
       
        //Debug.Log(transform.right.x);
        currentHealth = maxhealth;
        rb = GetComponent<Rigidbody2D>();
        c2d = GetComponent<Collider2D>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        }
        Alive();
    }

    // Update is called once per frame
    void Update()
    {
        if (working && Mathf.Abs(player.position.x - transform.position.x) < 50)
        {
            GroundCheck();
            if (rb.velocity.y <= 0) isJumping = false;
            if (grounded && !isJumping)
                if (jumpTimer <= 0.001f)
                {
                    
                    float delay = UnityEngine.Random.Range(jumpMinCooldown, jumpMaxCooldown);
                    float time = Time.time;
                    _Anim.SetTrigger("Landing");
                    jumpTimer = time + delay;
                    turnTimer = time + delay* 3 / 4;
                    //Debug.Log(string.Format("Current time {0} with delay{1} timer {2}", time, delay,jumpTimer));
                }
                else {
                    if(turnTimer>Time.time)
                        if (player.position.x>transform.position.x)
                        {
                            transform.right = Vector2.right;
                        }
                        else
                        {
                            transform.right = Vector2.left;
                        }
                    Jump();
                }

                
        }
    }
    private void GroundCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, ground_detection_distance, ground);
        if (hit)
        {
            grounded = true;
            if(rb.velocity.y<0)
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        else
        {
            grounded = false;
        }
        _Anim.SetBool("isOnGround", grounded);
    }
    private void Jump()
    {
        if (jumpTimer > Time.time)
        {
            //Debug.Log("hi!");
            return;
        }/*
        else
        {
            Debug.Log(string.Format("Timer {0} and time {1}", jumpTimer, Time.time));
        }*/
        boinkSource.Play();
        _Anim.SetTrigger("Jump");
        isJumping = true;
        if (player.position.x > transform.position.x)
        {
            rb.velocity = new Vector2(horizontal_force, up_force);
        }
        else {
            rb.velocity = new Vector2(-horizontal_force, up_force);
        }
        jumpTimer = 0;
        jumpRight=!jumpRight;
    }
    public void TakeDamage()
    {
        --currentHealth;
        OnHealthChange((float)currentHealth / (float)maxhealth);
        if (currentHealth <= 0)
        {
            Limbo();
            _Anim.SetBool("Death", true);
            Destroy(this.gameObject,1);
        }
    }
    public void TakeDamage(int i,bool fromLeft=true)
    {
        if (fromLeft)
        {
            if(transform.right.x>0)
                _Anim.SetTrigger("DamageBack");
            else
                _Anim.SetTrigger("DamageFront");
        }
        else
        {
            if (transform.right.x > 0)
                _Anim.SetTrigger("DamageFront");
            else
                _Anim.SetTrigger("DamageBack");
        }
        --currentHealth;
        OnHealthChange((float)currentHealth / (float)maxhealth);
        if (currentHealth <= 0)
        {
            Death();
        }
    }
    private void Death()
    {
        Limbo();
        _Anim.SetBool("Death", true);
        Destroy(this.gameObject);

    }
    public void Alive()
    {
        working = true;
        c2d.enabled = true;
        rb.gravityScale = 1;
    }

    public void Limbo()
    {
        working = false;
        c2d.enabled = false;
        rb.gravityScale = 0;
        rb.velocity = Vector3.zero;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Tags.Player))
        {
            if (collision.gameObject.GetComponent<Movement2D>().isSwinging)
            {
                Destroy(this.gameObject);
            }
            else
            {
                collision.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Death))
            Death();
    }

    public void TakeDamage(int i)
    {
        throw new NotImplementedException();
    }
}
