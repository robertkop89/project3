﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseAi : MonoBehaviour,IEnemy
{
    public Transform targetHole;
    public float speed=10;
    bool alive = true;
    private Renderer ren;
    private Rigidbody2D rb;
    private Collider2D c2d;
    private static Transform player;
    [SerializeField]
    Animator _anim;
    [SerializeField]
    AudioSource _movementSound;
    [SerializeField]
    AudioSource _hitSound;

    public event Action<float> OnHealthChange = delegate { };

    public void TakeDamage()
    {
        Destroy(this.gameObject);
    }
    public void TakeDamage(int i)
    {
        Limbo();
        
        _anim.SetTrigger("Damage");
        OnHealthChange(0);
        _anim.SetBool("Death", true);
        Destroy(this.gameObject,1);
    }

    // Start is called before the first frame update
    void Awake()
    {
        c2d = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        ren = gameObject.GetComponentInChildren<Renderer>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    private void Start()
    {
        if (targetHole != null)
        {
            transform.right = new Vector3(targetHole.position.x - transform.position.x, 0, 0);
            //Debug.Log(transform.right);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(alive && targetHole != null && ((player.position.x - transform.position.x) < 10 || (player.position.x - transform.position.x) >-50))
        {
            if (!_movementSound.isPlaying) _movementSound.Play();
            transform.right = new Vector3(targetHole.position.x - transform.position.x, 0, 0);
            if (transform.position.x - targetHole.position.x > 1)
            {
                transform.position += new Vector3(transform.right.x * Time.deltaTime*speed, 0, 0);
            }
            else
            {
                alive = false;
                Destroy(this.gameObject);
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            ren.material.SetColor("_Color", Color.yellow);
            TakeDamage(1);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            if(!_hitSound.isPlaying)
            _hitSound.Play();
            collision.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
        }
    }
    public void Alive()
    {
        _anim.SetTrigger("Run");
        _anim.SetFloat("Speed", 1);
        c2d.enabled = true;
        rb.gravityScale = 1;
        alive = true;
    }

    public void Limbo()
    {
        _anim.SetTrigger("Idle");
        _anim.SetFloat("Speed", 1);
        _movementSound.Stop();
        c2d.enabled = false;
        rb.gravityScale = 0;
        alive = false;
    }

    
}
