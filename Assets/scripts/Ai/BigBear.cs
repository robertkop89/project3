﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BigBear : BearAi
{
    public GameObject GameOverScreen;
    public new event Action<float> OnHealthChange = delegate { };
    public new void TakeDamage(int i)
    {
        Debug.Log(i);
        if (action)
            animator.SetTrigger("Attacked");
        currentHealth--;
        OnHealthChange((float)currentHealth / (float)maxHealth);
        if (currentHealth <= 0)
        {
            deathSound.Play();
            MusicController.Instance.Release(99);
            Limbo();
            Time.timeScale = 0;
            GameOverScreen.SetActive(true);
        }
    }
}
