﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class MouseHoleScript : MonoBehaviour
{
    [SerializeField]
    GameObject mouse;
    [SerializeField]
    public Transform targetHole;
    public GameObject currentMouse;
    public bool sleeping = false;


    // Update is called once per frame
    void Update()
    {
        if (sleeping) return;
        else
        {
            if (currentMouse == null)
            {
                currentMouse = Instantiate(mouse, transform.position, transform.rotation);
                currentMouse.GetComponent<mouseAi>().targetHole = targetHole;
            }
        }
    }
    public void Live()
    {
        sleeping = false;
    }
    public void Suspend()
    {
        sleeping = true;
    }
}
