﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour, IEnemy
{
    [SerializeField]
    int maxHealth = 100;
    int currentHealth;
    [SerializeField]
    Image healthBar;

    public event Action<float> OnHealthChange = delegate { };

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    private void RestoreHealth()
    {
        currentHealth = maxHealth;
        if(healthBar!=null)
        healthBar.fillAmount = 1;
    }
    
    public void TakeDamage(int damage)
    {
        if (currentHealth > 0)
            currentHealth -= damage;
        else currentHealth = 0;
        //Play animation
        if(healthBar!=null)
        healthBar.fillAmount =(float) currentHealth / maxHealth;
        
        if( currentHealth <= 0)
        {
            Die();
            //RestoreHealth();
        }
    }
    void Die()
    {
        Debug.Log("Died");
        Destroy(this.gameObject);
    }

    public void Alive()
    {
        throw new System.NotImplementedException();
    }

    public void Limbo()
    {
        throw new System.NotImplementedException();
    }

    public void TakeDamage()
    {
        
    }
}
