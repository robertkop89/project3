﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearAi : MonoBehaviour,IEnemy
{
    public bool isGettingTied;
    public bool isGettingToppleOver;
    private static Transform player;
    public int maxHealth = 5;
    protected int currentHealth;
    State currentState;
    Vector3 startingPoint;
    // Start is called before the first frame update
    [SerializeField]
    protected Animator animator;
    public bool action;

    private Collider2D c2d;
    private Rigidbody2D rb;
    public
    AudioSource deathSound;
    public
    AudioSource bearRoar;
    public
    AudioSource bearStep;
    public
    AudioSource bearAttack;
    
    [Range(0, 1)]
    public float minBearStepVolume;
    [Range(0, 1)]
    public float maxBearStepVolume;
    public event Action<float> OnHealthChange = delegate { };

    void Awake()
    {
        startingPoint = transform.position;
        if (player is null)
        {
            player = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        }
        currentState = new Idle(this.gameObject, player,startingPoint,animator);
        c2d = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        currentState = currentState.Process();
        if (isGettingTied) { 
            currentState.TiedByPlayer();
            isGettingTied = false;
        }
        if (isGettingToppleOver)
        {
            isGettingToppleOver = false;
            currentState.ToppledOver();
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Tags.Player))
        {
            //if (collision.gameObject.GetComponent<Movement2D>().isSwinging) { };
            collision.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
        }
    }

    public void TakeDamage()
    {
        if (currentState.name == State.STATE.TOPPLED)
        {
            TakeDamage(1);
        }
    }

    public void TakeDamage(int i)
    {
        if(action)
            animator.SetTrigger("Attacked");
        currentHealth--;
        OnHealthChange((float)currentHealth / (float)maxHealth);
        if (currentHealth <= 0)
        {
            deathSound.Play();
            MusicController.Instance.Release();
            Limbo();
            Destroy(this.gameObject,1);
        }
    }

    public void Alive()
    {
        c2d.enabled = true;
        rb.gravityScale = 1;
        currentState.Awaken();
    }

    public void Limbo()
    {
        c2d.enabled = false;
        rb.gravityScale = 0;
        currentState.Sleep();
    }
}
