﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterAI : MonoBehaviour,IEnemy
{
    [SerializeField]
    private LayerMask groundLayer;
    CapsuleCollider2D rotorCollider;
    BoxCollider2D bodyCollider;
    bool alive = true;
    public int maxHealth = 2;
    private int currentHealth;
    private static Transform player;
    public float knockbackPower = 10f;
    public float margin = 1f;
    public float speed = 2f;
    [SerializeField]
    AudioSource deathSound;
    [SerializeField]
    AudioSource rotorSound;

    public event Action<float> OnHealthChange = delegate { };
    private bool IsGrounded()
    {
        float extraSize = 0.25f;
        RaycastHit2D raycasthit = Physics2D.BoxCast(bodyCollider.bounds.center, 
            bodyCollider.bounds.size, 0f, Vector2.down, extraSize, groundLayer);
        Color debugColor;
        if(raycasthit.collider != null)
        {
            debugColor = Color.green;
        }
        else
        {
            debugColor = Color.red;
        }
        Debug.DrawRay(bodyCollider.bounds.center + new Vector3(bodyCollider.bounds.extents.x, 0), Vector2.down * (bodyCollider.bounds.extents.y + extraSize), debugColor);
        Debug.DrawRay(bodyCollider.bounds.center - new Vector3(bodyCollider.bounds.extents.x, 0), Vector2.down * (bodyCollider.bounds.extents.y + extraSize), debugColor);
        Debug.DrawRay(bodyCollider.bounds.center - new Vector3(bodyCollider.bounds.extents.x, bodyCollider.bounds.extents.y+extraSize), Vector2.right * (bodyCollider.bounds.extents.x), debugColor);


        return raycasthit.collider !=null;
    }
    private void Start()
    {
        currentHealth = maxHealth;
        Alive();
    }
    public void Alive()
    {
        rotorCollider.enabled = true;
        bodyCollider.enabled = true;
        alive = true;
    }

    public void Limbo()
    {
        rotorCollider.enabled = false;
        bodyCollider.enabled = false;
        alive = false;
    }

    public void TakeDamage()
    {
        
    }

    public void TakeDamage(int i)
    {
        //Debug.Log("hey...");
        currentHealth -= i;
        OnHealthChange((float)currentHealth / (float)maxHealth);
        if (currentHealth <= 0)
        {
            Limbo();
            deathSound.Play();
            Destroy(this.gameObject,1);
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        rotorCollider = GetComponent<CapsuleCollider2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
        if (player is null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            
            collision.GetComponent<playerHealth>().TakeDamage(transform);
            //Debug.Log( collision.transform.position- transform.position);
            //collision.GetComponent<Movement2D>().GetRigidBody().velocity =
            //    (collision.transform.position - transform.position).normalized * knockbackPower;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.CompareTag(Tags.Player))
            collision.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
    }
    // Update is called once per frame
    void Update()
    {
        if (alive && Mathf.Abs(player.position.x - transform.position.x) < 50)
        {
            if (!rotorSound.isPlaying )
            {
                rotorSound.Play();
            }
            if(Mathf.Abs(player.position.x - transform.position.x  )>5)
            transform.right = new Vector3(player.position.x - transform.position.x, 0, 0);
            if (player.transform.position.y - transform.position.y > margin)
            {
                transform.position += speed * Time.deltaTime * Vector3.up;
            } else if
                (player.transform.position.y - transform.position.y < -margin)
            {
                if (!IsGrounded())
                transform.position += speed * Time.deltaTime * Vector3.down  ;
            }

               
        }
    }
}
