﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowardCamera : MonoBehaviour
{
    Camera _camera;
    private void Awake()
    {
        _camera = Camera.main;
    }
    void Update()
    {
        transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);
    }
}
