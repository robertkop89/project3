﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField] float fireCooldown=2;
    float fireTimer;
    [SerializeField] GameObject BulletPrefab;
    [SerializeField] Transform FirePosition;

    //[SerializeField] LineRenderer lr;
    Transform bullet;

    // Start is called before the first frame update
    void Start()
    {
        fireTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            if (bullet == null && fireTimer<Time.time)
            {
                fireTimer = Time.time + fireCooldown;
                bullet = Instantiate(BulletPrefab, transform.position, transform.rotation).transform;
                
                
            }else
            {
                if(bullet)
                    Destroy(bullet.gameObject);
            }
            
        } 

    }
    /*
    private void LateUpdate()
    {
        if (bullet != null)
        {
            lr.enabled = true;
            lr.positionCount = 2;
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, bullet.position);
        }else
        {
            if (lr.enabled)
            {
                lr.positionCount = 0;
                lr.enabled = false;
            }
        }
    }
    */
}
