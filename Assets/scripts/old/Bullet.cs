﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float speed = 20f;
    [SerializeField]
    float life = 2;
    [SerializeField]
    float pullForce = 10;
    [SerializeField]
    float pullVectorHeight = 0.1f;
    bool hit = false;
    void Awake()
    {
        Invoke("DestroyMe", life);
    }
    
    void FixedUpdate()
    {
        if (!hit)
        {
            transform.position += transform.forward * Time.deltaTime * speed;
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        hit = true;
        GameObject target = collision.collider.gameObject;
        if (target.layer == LayerMask.NameToLayer("Enemy"))
        {
            Rigidbody targetBody = target.GetComponent<Rigidbody>();
            if ( targetBody!= null)
            {
                CancelInvoke("DestroyMe");
                FixedJoint targetJoint = target.GetComponent<FixedJoint>();
                if (targetJoint!=null)
                targetJoint.connectedBody = GetComponent<Rigidbody>();
                Vector3 direction = transform.forward*-1;
                direction.y += pullVectorHeight;
                targetBody.AddForce(direction * pullForce,ForceMode.VelocityChange);
                GetComponent<BoxCollider>().enabled = false;
                Invoke("DestroyMe", 1);
            }
        }
    }
    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
