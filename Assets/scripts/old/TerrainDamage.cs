﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDamage : MonoBehaviour
{
    [SerializeField]
    int damage = 2;
    List<GameObject> victims;
    // Start is called before the first frame update
    void Start()
    {
        victims = new List<GameObject>();
        TimeTickSystem.OnTick += delegate (object sender, TimeTickSystem.OnTickEventArgs e)
         {
             GiveDamage();
         };
    }
    private void GiveDamage()
    {
        Debug.Log("Give");
        foreach(GameObject victim in victims)
        {
            IEnemy target = victim.GetComponent<IEnemy>();
            target.TakeDamage(damage);
            Debug.Log("Damage");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter");
        if (other.gameObject.GetComponent<MonoBehaviour>() is IEnemy)
        {
            Debug.Log("Got one");
            victims.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<MonoBehaviour>() is IEnemy)
        {
            victims.Remove(other.gameObject);
        }
    }

}
