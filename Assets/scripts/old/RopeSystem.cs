﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSystem : MonoBehaviour
{
    bool _is_hooked=false;
    bool valid = false;
    Rigidbody _rb;
    HingeJoint theJoint;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            if (_is_hooked)
            {
                _is_hooked = false;
                Destroy(theJoint);
            }
            else
            valid = true;

        }
        else valid = false;
    }
    private void OnTriggerStay(Collider other)
    {
        switch (other.tag)
        {
            case "hook":
                if (valid)
                {
                  
                        _is_hooked = true;

                        Vector3 distance = other.transform.position - gameObject.transform.position;
                        //_rb.AddForce(distance.normalized * 10, ForceMode.VelocityChange);
                        Debug.Log(distance+" "+distance.magnitude);
                        
                        theJoint = gameObject.AddComponent<HingeJoint>();
                        theJoint.autoConfigureConnectedAnchor = false;
                        theJoint.connectedBody = other.GetComponent<Rigidbody>();
                        theJoint.connectedAnchor = Vector3.zero;
                        theJoint.axis = Vector3.forward;
                        theJoint.anchor = new Vector3(0, distance.magnitude, 0);
                        Debug.Log("correct");
                    
                }
                    
                break;
        }
    }
}
