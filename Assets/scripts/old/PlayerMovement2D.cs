﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement2D : MonoBehaviour
{
    Rigidbody rgbody;
    [SerializeField]
    float speed=5f,jumpHeight=2f, groundDistance=0.4f, runningMultiplier=2, runningJumpMultiplier=5, walkingJumpMultiplier=3, standingJumpMultiplier=2;
    Transform groundCheck,attackPosition;
    bool isGrounded;
    public LayerMask ground;
    Vector3 squat= new Vector3(1, 0.5f, 1);
    bool isCrossing = false, running =false;

    Vector3 inputs = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        rgbody = GetComponent<Rigidbody>();
        groundCheck = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, ground, QueryTriggerInteraction.Ignore);
        
        inputs.x = Input.GetAxis("Horizontal");
        if (Input.GetButton("Run"))
        {
            running = true;
            inputs.x *= runningMultiplier;
        }
        if (isCrossing)
        {
            inputs.z = Input.GetAxis("Vertical");
            if (inputs.z != 0)
            {
                if (running) inputs.z *= runningMultiplier;
                inputs.x = 0;
            }
        }

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            if (inputs != Vector3.zero)
            {
                if(running)
                    rgbody.AddForce(Vector3.up * Mathf.Sqrt(jumpHeight * -1 * runningJumpMultiplier * Physics.gravity.y), ForceMode.VelocityChange);
                else
                    rgbody.AddForce(Vector3.up * Mathf.Sqrt(jumpHeight * -1 * walkingJumpMultiplier * Physics.gravity.y), ForceMode.VelocityChange);
            }
            else
            rgbody.AddForce(Vector3.up * Mathf.Sqrt(jumpHeight* -1 * standingJumpMultiplier * Physics.gravity.y),ForceMode.VelocityChange);
        }
        if (Input.GetButton("Crouch")) {
            transform.localScale = squat;
        }
        else
        {
            transform.localScale = Vector3.one;
        }
        if(inputs!=Vector3.zero)
        transform.right = inputs;
        
    }
    private void FixedUpdate()
    {
        if (!isCrossing)
        {
            inputs.z = 0;
        }
        rgbody.MovePosition(rgbody.position + inputs*speed*Time.deltaTime);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Crossing")
            isCrossing = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Crossing")
        {
            isCrossing = true;
        }
    }
}
