﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField]
    Transform position;
    void Start()
    {
        transform.LookAt
        (transform.position -position.transform.position,
        Vector3.up);
    }
}
