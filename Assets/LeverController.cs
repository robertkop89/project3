﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverController : MonoBehaviour
{
    [SerializeField]
    Material color1;
    [SerializeField]
    Material color2;
    [Tooltip("What to move")]
    [SerializeField]
    GameObject movedObject;
    [Tooltip("Where to move")]
    [SerializeField]
    Transform movedLocation;
    [SerializeField]
    float speed = 10f;
    bool moveToEnd = true;
    Vector3 startLocation;
    Vector3 endLocation;
    // Start is called before the first frame update
    void Start()
    {
        startLocation = movedObject.transform.position;
        endLocation = movedLocation.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
