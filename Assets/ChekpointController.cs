﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChekpointController : MonoBehaviour
{
    LineRenderer Beacon;
    SpriteRenderer _sp;
    LineRenderer beaconRenderer;
    BoxCollider2D triggerZone;
    // Start is called before the first frame update
    void Start()
    {
        triggerZone = GetComponent<BoxCollider2D>();
        _sp = GetComponent<SpriteRenderer>();
        beaconRenderer = GetComponent<LineRenderer>();
        /*Vector3 positon = transform.position + Vector3.down*triggerZone.size.y/2;
        beaconRenderer.SetPosition(0, positon);
        positon = transform.position + Vector3.up * triggerZone.size.y;
        beaconRenderer.SetPosition(1, positon);*/
        _sp.enabled = false;


        RespawnController.Instance.SpawnChange += CheckRespawn;
        CheckRespawn(RespawnController.Instance.GetCurrentRespawn());

    }
    void CheckRespawn(Vector2 target)
    {
        if ((Vector2)transform.position == target)
        {
            beaconRenderer.startColor = Color.black;
            beaconRenderer.endColor = Color.red;
        }
        else
        {
            beaconRenderer.startColor = Color.black;
            beaconRenderer.endColor = Color.blue;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            RespawnController.Instance.SetRespawn(transform);
        }
    }
}
