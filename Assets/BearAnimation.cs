﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearAnimation : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(Tags.Player);
    }

    Vector3 attackPosition;
    private void Update()
    {
        if (transform.position.x < player.transform.position.x)
            attackPosition = transform.position + new Vector3(3, 1, 0);
        else
            attackPosition = transform.position - new Vector3(3, 1, 0);
    }
    public void Punch()
    {
        Collider2D[] attacked = Physics2D.OverlapCapsuleAll(attackPosition, new Vector2(2.5f, 4f), CapsuleDirection2D.Vertical, 0);
        foreach (Collider2D a in attacked)
        {
            if (a.gameObject.CompareTag("Player"))
            {
                a.gameObject.GetComponent<playerHealth>().TakeDamage(transform);
            }
        }
        //anim.SetTrigger("isAttacking");
    }
}
