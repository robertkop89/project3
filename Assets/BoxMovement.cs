﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMovement : MonoBehaviour
{
    Rigidbody2D _rb;
    [SerializeField]
    float grabDistance = 1f;
    [SerializeField]
    LayerMask boxMask;
    GameObject box;
    FixedJoint2D fixJ;
    PushableBox pushBox;
    Vector3 aim;
    [SerializeField]
    GameObject Crosshair;
    [SerializeField]
    float crosshairDistanceMultiplier = 10;
    Transform targetTransform;
    bool manualControl;
    

    // Start is called before the first frame update
    void Start()
    {
        _rb=this.GetComponentInParent<Rigidbody2D>();
        manualControl = true;
    }
    private int FacingSign
    {
        get
        {
            Vector3 perp = Vector3.Cross(transform.forward, Vector3.right);
            float dir = Vector3.Dot(perp, transform.up);
            return dir > 0f ? -1 : dir < 0f ? 1 : 0;


        }
    }
    // Update is called once per frame
    void Update()
    {
        //aim=new Vector3(Input.GetAxis("HorizontalAim") , Input.GetAxis("VerticalAim"), 0);
        //if (aim.magnitude > 0.1f)
        //{
        //    aim.Normalize();
        //    aim *= crosshairDistanceMultiplier;
        //    aim += Vector3.forward * 0.15f;
        //    targetTransform = Crosshair.transform;
        //    Crosshair.transform.localPosition = aim;
        //    Crosshair.SetActive(true);
        //}
        if (manualControl)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * transform.localScale.x, grabDistance, boxMask);
            if (hit.collider != null && hit.collider.gameObject.tag == "pushable" && Input.GetButtonDown("Interact"))
            {
                box = hit.collider.gameObject;
                SetTargetOnBox();
            }
            else if (Input.GetButtonUp("Interact"))
            {
                ReleaseTarget();
            }
        }
        
    }
    public void GiveTarget(GameObject target)
    {
        box = target;
        manualControl = false;
        SetTargetOnBox();
    }
    private void SetTargetOnBox()
    {
        fixJ = box.GetComponent<FixedJoint2D>();
        fixJ.connectedBody = _rb;
        fixJ.enabled = true;
        pushBox = box.GetComponent<PushableBox>();
        pushBox.beingPushed = true;
    }
    public void ReleaseTarget()
    {
        manualControl = true;
        if (fixJ != null)
            fixJ.enabled = false;
        if (pushBox != null)
            pushBox.beingPushed = false;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + transform.right * transform.localScale.x * grabDistance);
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, aim*10);
    }

}
